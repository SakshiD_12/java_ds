
//WAP TO PRINT THE SQUARE OF THE EVEN DIGIT OF NUMBER

class Number{

	public static void main(String[] args){

		int N=82395;  //here 8 and 2 are even hence it will show 64 and 4
		while(N != 0){

			int rem= N%10;
			N=N/10;
			if(rem%2==0){

				System.out.println(rem*rem);

			}
		}
	}
}

