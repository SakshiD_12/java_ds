
//------------------------->LEFT SHIFT (<<) AND RIGHT SHIFT (UNSIGNED RIGHT SHIFT >>) OPERATOR<-------------------------------------
class Bitwise{

	public static void main(String[] args){
		
		int x=5; //0101
		int y=7; //0111

		System.out.println(x<<2);  //20
		System.out.println(y>>2);  //1

	}
}


