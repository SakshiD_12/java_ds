
//----------------------------------------->ZERO FILL COMPLEMENT RIGHT SHIFT OPERATOR(>>>)<---------------------------------------------------
//1)IT WORKS ON NEGATIVE I/P
//THE RIGHT SHIFT OPERATOR AND ZERO FILL OPERATOR GIVES SAME O/P FOR POSITIVE I/P
class Bitwise{

	public static void main(String[] args){
		
		int x=7; //0111

		System.out.println(x>>2);  //1
		System.out.println(x>>>2);  //1

	}
}


