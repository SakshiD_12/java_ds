
//----------------------------------------------------->XOR(^)<--------------------------------------------------------
//     p        q         p^q
//     0        0          0
//     0        1          1
//     1        0          1
//     1        1          0
class Bitwise{

	public static void main(String[] args){
		
		int x=5; //0101
		int y=7; //0111

		System.out.println(x | y); //0111 =7
		System.out.println(x & y); //0101=5
		System.out.println(x ^ y); //0010=2

	}
}


