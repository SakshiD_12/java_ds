
//------------------------------------------------> ternary ( ? : ) <---------------------------------------------------------
class Ternary{

	public static void main(String[] args){
		
		int x = 10;
		int y = 20;

		System.out.println( (x<y) ? x:y); // means if x is less than y ? if yes prints x else y
		                                 //this is called ternary cuz it has 3 operands
		                                 // 1) x<y
						 // 2)x
						 // 3)y


	}
}
