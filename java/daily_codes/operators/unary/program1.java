
//******************PRE-INCREMENT AND PRE-DECREMENT*****************8
class Unary{

	public static void main(String[] args){

		int x=10;
		int y=20;

		System.out.println(++x); //11
		System.out.println(++y); //21

		System.out.println(--x); //10
		System.out.println(--y); //20

		System.out.println(x); //10
		System.out.println(y); //20

	}
}
