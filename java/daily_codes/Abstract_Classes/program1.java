

//ABSTRACT CLASSES : ABSTRACTION CAN BE ACHIEVE USING THE ABSTRACT CLASSES 
// USING ABSTRACT CLASS CLASS CAN BE MADE 0% TO 100% ABSTRACT 
// ABSTRACT CLASSES CANNOT HAVE OBJECTS
// BUT CAN HAVE CONSTRUCTOR TO INITIALIZE INSTANCE METHODS AND VARIABLE OF THAT CLASS 
// TO DECLARE METHOD AS ABSTRACT......abstract keyword is used 
// if class contains atleast one abstract method then we have to make that class abstract else it will give error

abstract class Parent{

	void career(){

		System.out.println("DOCTOR");

	}
	abstract void marry();

}
class child extends Parent{

	void marry(){

		System.out.println("Pratik shinde");

	}
}
class Client{
	public static void main(String[] args){

		child obj = new child();
		obj.career();
		obj.marry();

	}
}

