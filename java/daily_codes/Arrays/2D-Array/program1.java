
//*************************** 2D Array ****************************

//TAKING ELEMENTS FROM USER AND PRINTING IT
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		int arr[][] = new int[3][3]; //DECLARING ARRAY

		//TAKING ARRAY ELEMENTS FROM USER 
		System.out.println("ENTER DATA :");
		for(int i=0;i<arr.length;i++){
  
			for(int j=0;j<arr[i].length;j++){

				arr[i][j]=sc.nextInt();
			}

		}
		System.out.println("ARRAY ELEMENTS ARE :");
		for(int i=0;i<arr.length;i++){
  
			for(int j=0;j<arr[i].length;j++){

				System.out.print(arr[i][j]+" ");
			}
			System.out.println();

		}
		
		
	}
}

