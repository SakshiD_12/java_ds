
//*************************** 2D Array ****************************

//METHOD 2 :
class Demo{

	public static void main(String[] args){
		
		//ANOTHER WAY TO GIVING ELMENETS TO ARRAY( USING INITIALIZER LIST )
		

		int arr[][] = new int[][]{{10,20,30},{40,50,60},{70,80,90}}; //*************** ROW SIZE IS COMPULSORY IN ARRAY **********

		// int arr[][] = new int[3][]{{10,20,30},{40,50,60},{70,80,90}}; //THIS IS NOT ALLOWED (SIZE AND LIST BOTH NOT ALLOWED)

		System.out.println("ARRAY ELEMENTS ARE :");
		for(int i=0;i<arr.length;i++){
  
			for(int j=0;j<arr[i].length;j++){

				System.out.print(arr[i][j]+" ");
			}
			System.out.println();

		}
		
		
	}
}

