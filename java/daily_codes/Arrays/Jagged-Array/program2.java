
//PRINTING JAGGED ARRAY USING FOR EACH LOOP :
class JaggedArrayDemo{

	public static void main(String[] args){

		int arr[][] = new int[][]{{10,20,30},{40,50},{60}};

		for(int[] x : arr){

			for(int y : x){

				System.out.print(y+" ");

			}
			System.out.println();
		}
	}
}
