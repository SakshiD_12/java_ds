
//INITIALIZATION OF JAGGED ARRAY HAVING 2 METHODS :

class JaggedArrayDemo{

	public static void main(String[] args){

		//FIRST METHOD ---------------->

		int arr[][] = new int[][]{{10,20,30},{40,50},{60}};
		
		//SECOND METHOD --------------->
		
		int arr1[][] = new int[3][];

		arr1[0]= new int[3]; // 1ST ROW HAS 3 ELEMENTS
		arr1[1]= new int[2]; // 2ND ROW HAS 2 ELEMENTS 
		arr1[2]= new int[1]; 

	
	}
}
