
//TAKING ELEMENTS FROM USER IN JAGGED ARRAY :

import java.io.*;
class JaggedArrayDemo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[][] = new int[3][];

		arr[0]= new int[1]; // 1ST ROW HAS 3 ELEMENTS
		arr[1]= new int[2]; // 2ND ROW HAS 2 ELEMENTS 
		arr[2]= new int[1]; 
		
		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr[i].length;j++){

				arr[i][j]=Integer.parseInt(br.readLine());

			}
		}
		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr[i].length;j++){
 
				System.out.print(arr[i][j]+" ");

			}

			System.out.println();
		}
	
	
	}
}
