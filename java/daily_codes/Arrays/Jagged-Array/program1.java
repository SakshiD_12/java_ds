
//JAGGED ARRAY : JAGGED ARRAY IS SAME LIKE 2D ARRAY BUT IT HAS DIFFERENT NUMBER OF ELEMENTS IN EVERY COLOUMNS 
//  example : 10 20 30
//            40 50
//            70
//  JAGGED ARRAY COMES IN FREAME TO SAVE MEMEROY HENCE MEMORY MANAGEMENT IN JAVA IS BETTER
//  IN CPP AND C THIS IS NOT POSSIBLE HERE DEFAULT VALUE 0 IS ASSIGNED TO EVERY ARRAY BOX NO BOX KEPT EMPTY AS IN JAVA

class JaggedArrayDemo{

	public static void main(String[] args){

		int arr[][] = new int[][]{{10,20,30},{40,50},{60}};

		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr[i].length;j++){

				System.out.print(arr[i][j]+" ");

			}
			
			System.out.println();

		}
	}
}
