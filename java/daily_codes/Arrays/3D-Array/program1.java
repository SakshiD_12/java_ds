

//******************************************************** 3D - ARRAY ************************************************

class Demo{

	public static void main(String[] args){

		// int arr[][][]= new int[][][]; --------> DIMENSION MISSING ERROR 
		
		int arr[][][]= new int[2][][]; //-------->chalat

		// int arr1[][][]=new int[2][][]{{1,2,3},{4,5,6},{7,8,9},{10,11,12},{13,14,15},{16,17,18}}; --------> BOTH ARE NOT ALLOWED(size and list)

		int arr1[][][]=new int[][][]{{{1,2,3},{4,5,6},{7,8,9}},{{10,11,12},{13,14,15},{16,17,18}}};

		for(int i=0;i<2;i++){

			for(int j=0;j<3;j++){

				for(int k=0;k<3;k++){

					System.out.print(arr1[i][j][k]+" ");

				}

				System.out.println();
			}

		}

	}
}
