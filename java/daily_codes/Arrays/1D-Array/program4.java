
// IF WE GIVE ElEMENTS LESS THAN ARRAY SIZE THE IT WILL PRINT 0 BY DEFAULT 
class Demo{

	public static void main(String[] args){

		int arr[] = new int[4]; 

		arr[0]=10;
		arr[1]=20;
		arr[2]=30;
		
		System.out.println(arr[0]); //10
		System.out.println(arr[1]); //20
		System.out.println(arr[2]); //30
		System.out.println(arr[3]); //0
	}
}
