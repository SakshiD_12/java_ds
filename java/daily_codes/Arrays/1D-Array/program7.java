
//*********************************** DIFFERENT YTPES OF ARRAYS ********************************

class Demo{

	public static void main(String[] args){

		int arr[]={10,20,30,40};

		char arr2[]={'A','B','C'};

		float arr3[]={10.5f,20.5f};

		boolean arr4[]={true,false,true};

		System.out.println("************************* INTEGER ARRAY ***************************");
		System.out.println(arr[0]);
		System.out.println(arr[1]);
		System.out.println(arr[2]);
		System.out.println(arr[3]);
		System.out.println("************************* CHARACTER ARRAY ***************************");
		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		System.out.println(arr2[2]);
		System.out.println("************************* FLOAT ARRAY ***************************");
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		System.out.println("************************* BOOLEAN ARRAY ***************************");

		System.out.println(arr4[0]);
		System.out.println(arr4[1]);
		System.out.println(arr4[2]);
	}
}

