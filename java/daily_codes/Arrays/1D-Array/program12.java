

class Demo{

	public static void main(String[] args){

		int arr[]={10,20}; //------>internally int arr[]=new int[]{10,20};
		                  //EVRYTHING THAT INCLUDES NEW WILL GO ON HEAP SECTION AND WILL MAKE ITS IOBJECT
				  //ARRAY IS ONE TYPE OF CLASS HENCE NEW IS THE OBJECT OF THAT CLASS ON HEAP SECTION
				  //IN OUR ARRAY IT IS INTEGER CLASS ARRAY 

		System.out.println(arr); //if we print the name of array it will return the address of object which is on heap section
					//AS IT IS INTEGER CLASS ARRAY ADRESS STARTS WITH I

		float arr1[]={10.5f,15.2f};

		System.out.println(arr1);//-------> address starts with F (THIS ALL ARE VIRTUAL ADDRESSES NOT AN ACTUAL)

		char arr2[]={'A','B'};

		System.out.println(arr2);//-------->AB

		boolean arr3[]={true,false};

		System.out.println(arr3);//------> Z

		double arr4[]={10,30};

		System.out.println(arr4);//--------->D
	}
}
