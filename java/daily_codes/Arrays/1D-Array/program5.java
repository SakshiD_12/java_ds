
// newarray (bytecode)

class Demo{

	public static void main(String[] args){

		int arr1[]={10,20,30,40,50}; //******************* NO ERROR *********************
		                            //BECAUSE JAVA USES newarray bytecode which automatically adds new keyword to our arr1 and assigned
					    //size as per number of elements given 

		//BYTECODE OF arr1[] IS---------------------> newarray int
		//BYTECODE OF arr2[] IS---------------------> newarray int
		
		//both goes same internally hence no error 

		int arr2[]=new int[]{10,20,30,40,50};

	}
}
