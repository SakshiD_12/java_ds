
//PASSING ARRAY AS AN ARGUMENT (WHEN WE PASS AN ARRAY IS PASSES THE ADDRESS OF ARRAY AND WHATEVER VHANGES WE MADE TO THE ARRAY OF THAT  ADDRESS IS
//REFLECT EVERYWHERE 

class Demo{

	static void fun(int xarr[]){
		
		for(int x: xarr){

			System.out.println(x);
		}

		xarr[0]=500;

	}
	public static void main(String[] args){

		int arr[]={10,20,30,40};

		for(int x : arr){

			System.out.println(x);

		}

		fun(arr); //------->passing array name means passing address of array on heap section 
		for(int x : arr){

			System.out.println(x);

		}

	}
}
