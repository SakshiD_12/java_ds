
//in java if we did not assign any values to the ***integer** array then by default the value is 0 and not a garbage value 
//for float by default value is 0.0

class Demo{

	public static void main(String[] args){

		int arr[]=new int[4];
		float arr1[]=new float[4];
		char arr2[]=new char[4];


		System.out.println(arr[0]); //0
		System.out.println(arr[1]); //0
		System.out.println(arr[2]); //0
		System.out.println(arr[3]); //0

		System.out.println(arr1[0]); //0.0
		System.out.println(arr1[1]); //0.0
		System.out.println(arr1[2]); //0.0
		System.out.println(arr1[3]); //0.0

 
	}
}
