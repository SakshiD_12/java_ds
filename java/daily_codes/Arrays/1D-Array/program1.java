
//ARRAYS ARE USED TO STORE MULTIPLR DATA OF SAME TYPE UNDER SAME VARIBLE NAME
//arrays in java is dynamic it goes on heap section to run
//new keyword is used to declare array in java
//JVM ALLOCATES MEMORY TO THE ARRAY AT RUNTIME 

class Demo{

	public static void main(String[] args){


		//**********************ARRAY DECLARATION****************************

		int arr[] = new int[4]; 

	//	int arr[] = new int[]; ----------------------> ERROR :array dimension missing
	
		//******************** ASSIGNING VALUES TO ARRAY ****************************
		
		//------------------1ST WAY
		arr[0]=10;
		arr[1]=20;
		arr[2]=30;
		arr[3]=40;

		//------------------2nd way

		//int arr[]=new int[]{10,20,30,40};

		//************************** ACCESSING ARRAY ELEMNTS********************

		System.out.println(arr[0]);
		System.out.println(arr[1]);
		System.out.println(arr[2]);
		System.out.println(arr[3]);

	}
}
