
//PASSING ARRAY AS AN ARGUMENT BUT NOT STORING RETURN VALUE IN ANY CONTAINER

class Demo{

	int fun(int x){

		int val=x+10;
		return val;

	}
	public static void main(String[] args){

		Demo obj = new Demo();

		obj.fun(10);

	}
}

//SOLUTION : NO ERROR 
