
//FOR EACH LOOP ONLY WORKS ON COLLECTION OF DATA AND TYPE OF DATA SHOULD BE CLASS ONLY ........
//USE FOR EACH LOOP ONLY WHEN WE HAVE TO PRINT THE ALL ELEMENTS OF ARRAY 
//WE cannot alter the sequence of array an print(ex : like we cannot print the odd index element or even element)
//IT ONLY WORKS FOR CLASS VALUES 
//IT TAKES TWO ARGUMENTS  1ST IS WHICH TYPE OF ARRAY DATA AND 2ND IS ARRAY NAME .

class ForEachDemo{

	public static void main(String[] args){

		int arr[]={10,20,30,40};
		int arr1[]={10,20,30,40};
		int arr2[]={10,20,30,40};

		for(int x : arr){

			System.out.println(x);

		}
		for(float y : arr1){

			System.out.println(y);

		}
		for(char z : arr2){  //ERROR : INCOMPATIBLE TYPES 

			System.out.println(z);

		}
	}
}
