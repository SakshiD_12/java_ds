

//THREE WAYS OF WRITING ARRAY

class Demo{

	public static void main(String[] args){
		
		//******************************************************* FIRST WAY ***************************************

		int arr[]=new int[4]; //----------> first declaring 
		
		arr[0]=10; //---------------> and then assigning values
		arr[1]=10;
		arr[2]=10;
		arr[3]=10;

		//********************************************** SECOND WAY ******************************************

		int arr1[]={20,20,20,20}; //------------------>DIRECT GIVING INITIALIZER LIST

		//********************************************* THIRD WAY  ***************************************

		int arr2[]=new int[]{30,30,30,30}; //---------->initializing at the time of declaration

		int arr3=new int[4]{40,40,40,40}; //-----> THIS WILL GIVE ERROR AS WE CAN ONLY DO ON AT A TIME SIZE OR LIST

	}
}
