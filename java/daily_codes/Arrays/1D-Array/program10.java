

//*****************TAKING ARRAY INPUT USING BUFFERED READER *********************

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));

		int arr[]=new int[4];

		System.out.println("ENTER ARRAY ELEMENTS :");

		for(int i=0;i<4;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		System.out.println("ARRAY ELEMENTS ARE :");

		for(int i=0;i<4;i++){

			System.out.println(arr[i]);
		}
	}
}
