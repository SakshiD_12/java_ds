
//*************************** TAKING INPUT FROM USER USING SCANNER CLASS ****************************

import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int arr[]=new int[4];

		for(int i=0;i<4;i++){

			System.out.println("ENTER " +i+ " th ELEMENT :");
			arr[i]=sc.nextInt();

		}

		System.out.println("ARRAY ELEMENTS ARE :");

		for(int i=0;i<4;i++){

			System.out.println(arr[i]);

		}
	}
}
