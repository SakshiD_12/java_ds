

// INTEGER CACHE : 1)IT IS INTRODUCED IN 1.5 VERSION 
//                 2)THE PURPOSE IS TO SAVE MONEY 
//Autoboxing.
class Demo{

	public static void main(String[] args){
	
		// JAVA int x LA INTERNALLY Integer x MADHE AUTOBOX KRTO 
		int x=10;  //----------> Internally it goes as Integer class( Integer x= 10; )
		int y=10;  //-----------> Internally ( Integer y=10; ) This concept is called autoboxing
		Integer z=10;
		
		//THE ADDRESS OF BELOW THREE VARIABLES IS SAME BECAUSE VARIABLE X IS INTERNALLY GOES AS INTEGER CLASS
		//LIKE THIRD VARIABLE HENCE IT WILL GIVE THE SAME ADDRESS FOR EACH OBJECT AS THE RANGE OF VALUE(10) IS
		//BEWTWEEN 1 BYTE HENCE THE ANS OF ALL IS SAME 
		System.out.println(System.identityHashCode(x)); 
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));

	}
}
