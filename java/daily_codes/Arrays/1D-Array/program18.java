
//PASSING ARRAY AS AN ARGUMENT BUT NOT WRITE INT AS RETURN TYPE
class Demo{

	void fun(int x){

		int val=x+10;
		return val;

	}
	public static void main(String[] args){

		Demo obj = new Demo();

		System.out.println(obj.fun(10));

	}
}

//SOLUTION : ERROR : UNEXPECTED RETURN VALUE 
