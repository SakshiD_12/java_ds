

//IF PARENT AND CHILD HAVE SAME METHODS OR VARIABLE THEN WE CAN USE SUPER KEYWORD TO ACCESS IT 

class Parent{

	int x=10;
	static int y=20;

	Parent(){

		System.out.println("IN PARENT");

	}
}
class Child extends Parent{

	int x=100;
	static int y=200;

	Child(){

		System.out.println("IN CHILD");

	}
	void access(){

		//System.out.println(super);  ------------------> we cannot print the super
		System.out.println(super.x);  //--------------> this will print the parent x variable	
		System.out.println(super.y);  //--------> this will print the parent y variable 
		System.out.println(x);    // or Sop(this.x);
		System.out.println(y);    // or sop(this.y)

	}
}
class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.access();

	}
}
