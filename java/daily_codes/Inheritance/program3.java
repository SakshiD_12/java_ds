
// CREATION OF OBJECT IS VERY HEAVY OPERATION SO INHERITANCE CONCEPT COMES IN FRAME WHERE ONLY ONE OBJECT IS CREATED AND PASS TO THE OTHER INHERTITED CLASS 
class Parent{
	Parent(){

		System.out.println(this);

	}
}
class Child extends Parent{

	Child(){

		System.out.println(this);

	}
}
class Client{

	public static void main(String[] args){

		Child obj2 = new Child();

	}
}

