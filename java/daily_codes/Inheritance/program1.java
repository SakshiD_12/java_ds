
// INHERITANCE : when child class inherit methods and variables of parent class and also add its own methods and variables then this concept is known as inheritance
//extends keywoed is used to achive the inheritance 

class Parent{
	Parent(){

		System.out.println("IN PARENT CONSTRUCTOR");

	}
	void ParentProperty(){

		System.out.println("MONEY,GOLD,FLAT,CAR");

	}
}
class Child extends Parent{

	Child(){

		System.out.println("IN CHILD CONSTRUCTOR");

	}
}
class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.ParentProperty();

	}
}

