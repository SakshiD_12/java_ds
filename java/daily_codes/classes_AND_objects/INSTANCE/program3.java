
//SEQUENCE: STATIC VARIABLE ----> STATIC BLOCK ----> STATIC METHODS ----> INSTANCE VARIABLE ----> INSTANCE BLOCK ----> CONSTRUCTOR ---->INSTANCE METHODS 
//MAIN WILL COME AFTER STATIC BLOCK 

class Demo{
	
	int x=10;
	static int y=20;

	Demo(){
		System.out.println("IN CONSTRUCTOR");

	}
	static{
		System.out.println("IN STATIC BLOCK 1");

	}
	{
		System.out.println("INSTANCE BLOCK 1");

	}
	public static void main(String[] args){

		System.out.println("IN MAIN");

		Demo obj = new Demo();
	}
	static{
		System.out.println("IN STATIC BLOCK 2");

	}
	{
		System.out.println("INSTANCE BLOCK 2");

	}
}
