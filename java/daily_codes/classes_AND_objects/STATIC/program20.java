//NOTE : WE CANNOT WRITE STATIC VARIABLE IN STATIC BLOCK,MAIN METHOD AND NOT IN NON STATIC BLOCK ALSO
//STATIC VARIABLE IS ALLOWED TO WRITTEN IN ONLY CLASS HENCE IT IS CALLED AS CLASS VARIABLE
//REASON : STATIC VARIABLE COMES BEFORE MAIN,STATIC AND NON STATIC HENCE IT IS NOT ALLOWED 

class Demo{

	static int x=10;                             //OKAY

	static{
		static int y=10;                     //ERROR

	}
	void fun(){

		static int z=20;                    //ERROR

	}
	static void gun(){
		static int p=50;
	}
}

