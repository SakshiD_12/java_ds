
//STATIC BLOCK : WE CAN CREATE OBJECT IN STATIC BLOCK
//             :SETUP CHYA TIME LA CH SAMJT VARIABLE LA KUTHE JAGA MILTE
//             :PN TYNA ACTUALLY JAGA TEVA MITE JEVA STACK FRAME PUSH HOTE TYA FUNCTION CHI
//             :STATIC BLOCK MAIN CHYA PN ADHI YETO METHOD AREA VR 
//
//PROOF :

class Demo{

	static{
		System.out.println("IN STATIC");

	}
	public static void main(String[] args){

		System.out.println("IN MAIN");

	}
}

//Q . why static block comes first ?
//ans : to initialized variables 
