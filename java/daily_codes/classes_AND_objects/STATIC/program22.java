
//WHAT IF THERE IS MULTIPLE STATIC BLOCKS and main in every class 
//here the FILE that we give to JVM WHICH CONTAINS MAIN THEN THAT STATIC BLOCK WILL EXECUTE FIRST ( STATIC BLOCK CANNOT BE INHERITED IT IS LIMITED TO CLASS )
//like if we give demo class to jvm it will execute static block in demo else in cient( if we give client class to jvm )
class Demo{

	static{
		System.out.println("STATIC BLOCK 1");
	}
	public static void main(String[] args){

		System.out.println("IN DEMO MAIN");
	}

}
class Client{

	static{
		System.out.println("STATIC BLOCK 2");
	}
	public static void main(String[] args){

		System.out.println("IN CLIENT MAIN");

	}
	static{
		System.out.println("STATIC BLOCK 3");
	}
}



