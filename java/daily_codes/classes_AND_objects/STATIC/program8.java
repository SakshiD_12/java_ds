
// CONSTRUCTOR : constructor is used to initializes non static or instance variable 
//CONSTRUCTOR CAM BE CREATED BY COMPILER IF NOT CREATED BY PROGGRAMER . IF WE CREATE THE OBJECT OF CLASS CONSTRUCTOR INVOKED AUTOMATICALLY
//ID CONSTRUCTOR IS CREATED BY JAVA THEN IT IS CALLED AS DEFAULT CONSTRUCTOR AND IF IT IS CREATED BY PROGGRAMMER THEN IT IS CALLED AS NO ARGUMENT CONS.
//NO ARGUMENT CONSTRUCTOR HAS HIDDEN PARAMETER CALLED this

class Core2Web{

	Core2Web(){ //------->if wr write constructor then it is called explicitly else written automatically by compiler  then it is called implicitly 

		System.out.println("IN CONSTRUCTOR");

	}
	int x = 10; //NO MATTER WHERE IS X WRITTEN IT WILL STORE ONLY IN CONSTRUCTOR 

	public static void main(String[] args){

		System.out.println("IN MAIN");

	}
}
