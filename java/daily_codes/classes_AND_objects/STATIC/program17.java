
//STATIC VARIABLE AND STATIC BLOCK

//NOTE : WE CAN CALL STATIC VARIABLE AND STATIC METHOD USING CLASS NAME AND WITHOUT CREATING OBJECT 
//     : every class has special structure and each special structure has 2 parts pointer to static block and 2nd part is addresses of static methods 
//     :if class do not contain any static variable then pointer to static block is null
//     :special structure is one type of array i.e array of addresses 
//     :non static things is stored on heap in the object (object contain the pointer to special structure) 
//     :jya class cha object ahe tyach class chya special structurr kade bght asto object
//

class Demo{

	static int x=10;
	static int y=20;

	static void display(){

		System.out.println(x);
		System.out.println(y);

	}
}
class Client{

	public static void main(String[] args){

		System.out.println(Demo.x);
		System.out.println(Demo.y);

		Demo.display();
	}
}
