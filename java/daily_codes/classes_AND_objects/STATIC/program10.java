
//NOTE : EVEN THE INSTANCE STRING STORE IN THE CONSTRUCTOR 

class Demo{

	int x=10;
	String str = "SHAHSI"; //SCP (BUT ADDRESS OF STR1 IS STORED IN CONSTRUCTOR)

	void fun(){

		String str2="Shashi"; //SCP (BUT ADDRESS IS STORED IN STACK FRAME OF fun() )
		String str3=new String("CORE2WEB"); //ON HEAP ( ADDRESS IS STORED IN STACK FRAMR OF fun() )

	}
	public static void main(String[] args){

		Demo obj = new Demo();
		obj.fun();

	}
}
