
//NOTE : WHEN WE MAKE OBJECT OF CLASS THEN CONSTRUCTOR IS CALLED AUTOMATICALLY 

class Demo{

	Demo(){
		System.out.println("IN DEMO CONSTRUCTOR");

	}
	public static void main(String[] args){

		Demo obj1=new Demo();
		Demo obj2=new Demo();
	}
}
//o/p : IN DEMO CONSTRUCTOR
//      IN DEMO CONSTRUCTOR
