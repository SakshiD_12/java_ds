
//WHAT IF THERE IS MULTIPLE STATIC BLOCKS
//GIVE THAT FILE TO JVM WHICH CONTAINS MAIN THEN THAT STATIC BLOCK WILL EXECUTE FIRST ( STATIC BLOCK CANNOT BE INHERITED IT IS LIMITED TO CLASS )
class Demo{

	static{
		System.out.println("STATIC BLOCK 1");
	}

}
class Client{

	static{
		System.out.println("STATIC BLOCK 2");
	}
	public static void main(String[] args){

		System.out.println("IN MAIN");

	}
	static{
		System.out.println("STATIC BLOCK 3");
	}
}



