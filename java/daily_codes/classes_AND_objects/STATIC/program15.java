
//NOTE :
// INSTANCE VARIABLE CHA SCOPE OBJECT PURTA LIMITED ASTO EKA OBJECT CHYA INSTANCE VARIABLE MADHE CHANGE KELA TR TO DUSRYA OBJECT LA DIST NAHI
// HECH STATIC VARIABLE MDHE CHANGE KELA TRI SGLYA OBJECT MADHE DISTO KARAN STATIC VARIABLE METHOD AREA VAR STORE HOTAT ANI METHOD AREA HE GLOBAL DATA STRUCTURE AHE MNUN 

//CODE :
class Employee {

	int id=101;
	String name="SAKSHI";
	int rank=1000;

	void empInfo(){

		System.out.println(id);
		System.out.println(name);
		System.out.println(rank);
	}
}
class Client{

	public static void main(String[] args){

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.empInfo();
		emp2.empInfo();

		emp2.id=202;
		emp2.name="sayali";
		emp2.rank=2000;

		System.out.println("AFTER CHANGING IN VARIABLE OF EMP2 :");
	
		//THE CHANGES WILL DONE IN EMP2 ONLY 

		emp1.empInfo();
		emp2.empInfo();

	}
}

		

