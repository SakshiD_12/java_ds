
//example of private access specifier 

class Core2Web{

	int noc=2;
	private String FC ="CPP";  //ACCESIBLE ONLY IN THE SAME CLASS 
	void dislay(){

		System.out.println(noc);
		System.out.println(FC);

	}
}
class User{

	public static void main(String[] args){

		Core2Web obj = new Core2Web();
		obj.display();
	
		System.out.println(obj.noc);
		System.out.println(obj.FC); //ERROR : FC HAS PRIVATE ACCESS IN CORE2WEB 

	}
}

