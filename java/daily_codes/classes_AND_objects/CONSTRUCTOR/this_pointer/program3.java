

//NOTE : CODE TO CALL ANOTHER CONSTRUCTOR FROM ONE CONSTRUCTOR BY ONLY MAKING ONE OBJECT AND USING this REFERENCE 

class Demo{

	int x=10;

	Demo(){

		System.out.println("IN NO ARGUMENT CONSTRUCTOR");

	}
	Demo(int x){
		
		this();  //---------------------------------------------->THIS WILL CALL ANOTHER CONSTRUCTOR 
	        //Demo obj = new Demo(); //----------------------------------->we can do this also but creation of object is very heavy process 
		System.out.println("IN PARAMETERISED CONSTRUCTOR");

	}
	public static void main(String[] args){

		Demo obj = new Demo(50);

	}
}
