
//NOTE : WE CAN CHANGE PRIVATE VARIABLE VALUE USING this OR getter/setter

// SETTER : SETTER METHOS IS ONE WHO CARRIES DATA FROM OUTSIDE AND CHANGES THE VALUE OF INSTANCE PRIVATE VARIABLE IN THE METHOD 


// GETTER : GTTER IS THE METHOD WHICH HELPS TO PRINT THE UPDATED DATA IN THE METHOD 

class Player{

	private int jerNo=0;
	private String name=null;

	Player(int jerNo,String name){

		this.jerNo=jerNo;
		this.name=name;

		System.out.println("IN PARAMETERISED CONSTRUCTOR");

	}
	void info(){

		System.out.println(jerNo+" = "+name);

	}
}
class Client{

	public static void main(String[] args){

		Player obj = new Player(18,"virat"); //--------------------> internally -----------> Player(obj , 18 , "virat"); 
		obj.info();

		Player obj1 = new Player(07,"MSD");
		obj1.info();

		Player obj2 = new Player(45,"Rohit"); 
		obj2.info();

	}
}
