//HIDDEN THIS POINTER : USING THIS WE CAN ACCESS THE METHODS AND VARIABLES OF CLASS IT IS AN IMPLICIT REFERENCE TO THE OBJECT

//ALL INSTANCE OR NON STATIC THINGS HAS HIDDEN THIS POINTER 

// Demo obj = new Demo() --------internally-------> Demo(obj) ithun obj pathvtoy mnje vrti ghenara class asla pahije (Demo this he bhand lavaych vrti)

// obj == this 

//PROOF :

class Demo{

	int x=10;
	Demo(){   //-------------internally---------------> Demo( Demo this )

		System.out.println("IN CONSTRUCTOR");
		System.out.println(x);
		System.out.println(this.x);

	}
	void fun(){ //--------internally-------->fun(Demo this)

		System.out.println(this);
		System.out.println(x);
		System.out.println(this.x);

	}
	public static void main(String[] args){

		Demo obj1 = new Demo(); //------internally------>Demo(obj)
		System.out.println(obj1);
		obj1.fun(); //-------internally------->fun(obj)

	}
}
