


//NOTE : THIS CALL SHOULD BE ALWAYS ON FIRST LINE 

class Demo{

	int x=10;
	Demo(){

		System.out.println(this.x); //10
		System.out.println(x);
		System.out.println("IN NO ARGUMENT CONSTRUCTOR");

	}
	Demo(int x){
		
		this.x=x;  //this line should be first always 
	//	this(); //error (this is to call another constructor)(as this is same as super is will call another constructor)
		System.out.println("IN PARAMETERIZED CONSTRUCTOR");
		System.out.println(this.x); //20
		System.out.println(x); //20

	}
	public static void main(String[] args){

		Demo obj = new Demo(); //THIS WILL CALL NO ARGUMENT CONSTRUCTOR 

		Demo obj1 = new Demo(20); //THIS WILL CALL PARAMETERIZED CONSTRUCTOR 

	}
}



