

//NOTE : RECURSIVE CONSTRUCTOR INVOCATION 

class Demo{

	int x=10;

	Demo(){
		this(50); //THIS WILL CALL PARAMETERIZED CONSTRUCTOR 
		System.out.println("IN NO ARGUMENT CONSTRUCTOR");

	}
	Demo(int x){
		
		this();  //---------------------------------------------->THIS WILL CALL NO ARGUMENT CONSTRUCTOR 
	      
		System.out.println("IN PARAMETERISED CONSTRUCTOR");

	}
	public static void main(String[] args){

		Demo obj = new Demo(50);

	}
}
