

//******************************************** METHOD TABLE ****************************************************

// THERE IS ONE TYPE OF TABLE CALLED METHOD TABLE WHICH STORES THE DATA TYPE(NOT A RETURN TYPE (THIS IS ONE THAT IS TYPE
// OF PARAMETER)) AND METHOD NAME i.e method signature 

//THIS TABLE IS MADE AT COMPILE TIME 

//METHOD SIGNATURE STORED IN METHOD TABLE 

//METHOD TABLE KEEPS TRACK OF ALL THE METHODS IN TABLE AND IF THE METHOD WITH SAME SIGNATURE FINDS IT WILL GIVE ERROR 

class Demo{
	public static void main(String[] args){

		void Demo(int x){  //------------>THIS WILL GOES IN TABLE AS Demo(int)


		}
		void Demo(float x){//-------->Demo(float) 


		}
	}
}
