
//*************************************** STRING TOKENIZATION CLASS **************************************

//TAKING VARIOUS TYPE OF INPUT FROM THE USER 

import java.io.*;
import java.util.*;

class StringDemo{

	public static void main(String[] args) throws IOException{ //------>HERE WE WRITE THROWS IOEXCEPTION TO CHECK WHETHER INPUT PIPE HAS A CONNECTION
		                                                  //WITH JVM OR NOT.(THIS EXCEPTION IS THROWN BY READLINE() METHOD).

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //COMBINING TWO CLASSES
		 

		System.out.println("ENTER PLAYER NAME,RUNS,GRADE,AVERAGE :");
		String info = br.readLine();
		
		StringTokenizer obj = new StringTokenizer(info," ");

		String pName = obj.nextToken();
		int runs = Integer.parseInt(obj.nextToken()); //to take int as input we have to type caste it 
		char grade = obj.nextToken().charAt(0); //to take character use charAt() method
		float avg = Float.parseFloat(obj.nextToken());

	
		System.out.println("PLAYER NAME :"+pName);
		System.out.println("TOTAL RUNS :"+runs);
		System.out.println("GRADE :"+grade);
		System.out.println("AVERAGE:"+avg);

	}
}

