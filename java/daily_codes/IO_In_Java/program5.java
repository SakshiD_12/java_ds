
//*************************************** STRING TOKENIZATION CLASS (USING BUFFEREDREADER) **************************************

//CLASS STRING TOKENIZATION :THIS CLASS IS USED TO MAKE TOKENS IN JAVA
//                          :MEANS WE CAN TAKE A MULTIPLE INPUT FROOM THE USER ON SINGLE LINE AND USING TOKENIZATION WE CAN PRINT THEM ON SEPERATE
//                          :LINES
//                          :IT TAKES TWO ARGUMENTS STRING NAME AND DLIMITER(EX:SPACE QUAMA ETC)
//                          :WE HAVE TO MAKE TOKENS FOR EVERY INPUT
//                          :STRING TOKENIZATION CLASS INCLUDES IN UTIL PACKAGE
//                          ;nextToken() method is used to dividethe string

import java.io.*;
import java.util.*;

class StringDemo{

	public static void main(String[] args) throws IOException{ //------>HERE WE WRITE THROWS IOEXCEPTION TO CHECK WHETHER INPUT PIPE HAS A CONNECTION
		                                                  //WITH JVM OR NOT.(THIS EXCEPTION IS THROWN BY READLINE() METHOD).

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //COMBINING TWO CLASSES
		 

		System.out.println("ENTER BUILDING NAME,FLAT NO,WING :");
		String info = br.readLine();
		
		StringTokenizer obj = new StringTokenizer(info," ");

		String token1=obj.nextToken();
		String token2=obj.nextToken();
		String token3=obj.nextToken();

	
		System.out.println("BUILDING NAME :"+token1);
		System.out.println("BUILDING FLAT_NO :"+token2);
		System.out.println("BUILDING WING :"+token3);

	}
}

