

//PROBLEM--------> WHAT IF WE MAKE MORE THAN OBJECT OF BR/ISR CLASS AND CLOSE ONE OF THE OBJECTS CONNECTION WITH KEYBOARD WHAT WILL HAPPEN.

//CONDITION 1 :IT WILL CLOSE THE CONNECTION FOR ONLY THAT ONE OBJECT

//CONDITION 2 :OR IT WILL CLOSE THE ACTUAL JVM PIPE OF SYSTEM.IN ?

import java.io.*;

class  Demo{

	public static void main(String[] args) throws IOException {

		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));

		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER NAME :");

		String str1=br1.readLine();

		System.out.println("STRING NAME :"+str1);

		br1.close();;

		String str2=br2.readLine();
		System.out.println("STRING NAME :"+str2);

	}
}

//SOLUTION : IF WE CLOSE ONE OF THE CONNECTION WITH KEYBOARD JVM ITSELF WILL CLOSE THE CONETION HENCE NONE OF THE OBJETC WILL ABLE TO TAKE THE 
//           INPUT
