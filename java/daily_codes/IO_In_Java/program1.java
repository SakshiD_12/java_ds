
//*****************************************************  USING SCANNER CLASS  ***************************************************************


import java.util.Scanner; //----------->SCANNER CLASS BELONGS TO UTIL PACKAGE
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in); //THIS SYSTEM.IN MAKES CONNECTION WITH KEYBOARD

		System.out.println("ENTER YOUR DREAM COMPANY :");
		String name = obj.next(); //------->IN JAVA TO TAKE STRING METHOD next() method is used

		System.out.println("ENTER PACKAGE :");
		float pack = obj.nextFloat(); 

	}
}
