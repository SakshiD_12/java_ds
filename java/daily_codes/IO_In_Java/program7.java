
//***************************************** STRING TOKENIZER USING SCANNER CLASS ************************************************


//-----> countTokens() method returns the count of tokens so that we can avoit making mistake like we we make 3 tokens and entered only 2
//       it will give error or if we entered input more than tokens it will also give error 
//-----> hasMoreTokens() check for the any of token is left or user finishes entering the token 
import java.util.*;

class StringDemo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("ENTER STUDENT INFO :");
		String str = sc.nextLine();

		StringTokenizer st = new StringTokenizer(str,",");
		System.out.println(st.countTokens()); //------------>RETURNS THE COUNT OF TOKENS WE ENTERED 

		//USING WHILE LOOP WE CAN AVOID WRITING NUMBER OR LOTS OF TOKENS 
		while(st.hasMoreTokens()){

			System.out.println(st.nextToken());

		}
	}
}
