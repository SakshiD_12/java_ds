

//*************************************************** USING BUFFERED READER CLASS******************************************************


import java.io.*; //--------->io package contains buffered reader and input stream reader

class Demo{

	public static void main(String[] args) throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in); //--->INPUTREADER CAN ONLY READ ONE CHARACTER HENCE BR ALWAYS COMES WITH ISR

		BufferedReader br = new BufferedReader(isr); //BR CAN STORE DATA BUT CANNOT DIRECTLY CONNECT TO THE KEYBOARD HENCE WE USE ISR IN BR

		System.out.println("ENTER NAME :");

//		char ch=(char)br.read(); //------>read() CAN ONLY READ ONE CHARACTER
//		                         //----->HERE WE HAVE TO TYPE CAST BECUZ READ METHOD HAVE INT RETURN TYPE 
 
                String ch=br.readLine(); //------>readline can read whole line upto \n
		                         //------>readline has return type String

		System.out.println("ENTER PERCENTAGE :");

		float percentage = Float.parseFloat(br.readLine()); //--->TO TAKE INTEGER AS INPUT WE HAVE TO USE RAPPER CLASS
		                                          //--->RAPPER CLASS WILL TYPE CAST USING PARSING


		System.out.println("ENTER ROLL NO :");

		int roll = Integer.parseInt(br.readLine());
		
		System.out.println("NAME :"+ch);
		System.out.println("PERCENTAGE :"+percentage);
		System.out.println("ROLL_NO :"+roll);
	}
}


