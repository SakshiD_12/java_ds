
//*************************************** SKIP METHOD IN BUFFERED READER CLASS **************************************


import java.io.*;

class Building{

	public static void main(String[] args) throws IOException{ //------>HERE WE WRITE THROWS IOEXCEPTION TO CHECK WHETHER INPUT PIPE HAS A CONNECTION
		                                                  //WITH JVM OR NOT.(THIS EXCEPTION IS THROWN BY READLINE() METHOD).

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //COMBINING TWO CLASSES
		 

		System.out.println("ENTER BUILDING NAME :");
		String name = br.readLine();
		
		System.out.println("ENTER WING :");
		char wing=(char)br.read();

		br.skip(1); //--->WE HAVE TO USE SKIP HERE BECAUSE WHEN WE ENTER THE WING AND CLICK ON THE ENTER THE REMAINING \N IN PIPE IS TAKEN BY 
		           //--->FLAT NO. AND TRYING TO CONVERT \N INTO INT WHICH WILL COZ AN EXCEPTION

		System.out.println("ENTER FLAT NUMBER :");
		int no= Integer.parseInt(br.readLine());

		System.out.println("BUILDING NAME :"+name);
		System.out.println("BUILDING WING :"+wing);
		System.out.println("BUILDING FLAT_NO :"+no);

	}
}

