

// METHOD LOCAL INNER CLASS : WHEN THERE IS CLASS PRESENT INSIDE THE METHOD THEN IT IS CALLED AS METHOD LOCAL INNER CLASS 

class Outer{

	void m1(){

		System.out.println("IN M1 OUTER");

		class Inner{
			
			void m2(){

			      System.out.println("In M2 INNER");

			}	      

		}

		Inner obj = new Inner();
		obj.m2();
	
	}
	void m2(){

		System.out.println("IN M2 OUTER");

	}
	public static void main(String[] args){

		Outer obj1 = new Outer();
		obj1.m1();
		obj1.m2();


	}
}
	
