
//WE CANNOT DIRECTLY WRITE STATIC VARIABLE INSIDE INNER CLASS WE HAVE TO DECLARE IT AS FINAL 

class Outer{

	int x=10;
	static int y=20;

	class Inner{

		final static int z = 30;

		void fun1(){

			System.out.println(x);
			System.out.println(y);
			System.out.println(z);

		}
	}
}
class Client{

	public static void main(String[] args){

		Outer.Inner obj = new Outer().new Inner();
		obj.fun1();

	}
}
