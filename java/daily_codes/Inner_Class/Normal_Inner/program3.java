
//FROM INNER CLASS WE CAN ACCESS STATIC AND NON STATIC THINGS OF OUTER CLASS 

class Outer{

	int x=10;
	static int y=20;

	class Inner{

		void fun2(){

			System.out.println(x);
			System.out.println(y);

		}
	}
	void fun1(){

		System.out.println("IN FUN1 OUTER");

	}
}
class Client{

	public static void main(String[] args){

		Outer obj = new Outer();
		obj.fun1();

		Outer.Inner obj1=obj.new Inner();
		obj1.fun2();


	}
}
