
/*  Syntax : class Outer{
 *
 *  			class Inner{
 *
 *
 *  			}
 *
 *  		}
 */
class Outer{

	class Inner{

		void m1(){

			System.out.println("IN M1 INNER ");

		}

	}
	void m2(){

		System.out.println("IN M2 OUTER");

	}
}
class Client{

	public static void main(String[] args){

		Outer obj=new Outer();
		obj.m2();

		Outer.Inner obj1 = obj.new Inner();  //object of inner class 
		obj1.m1();

		/*
		 or we can make object of inner class as 

		 Outer.Inner obj1 = new Outer().new Inner();

		 */

	}
}
