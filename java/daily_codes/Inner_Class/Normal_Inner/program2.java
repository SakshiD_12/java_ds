
//WHAT IF WE WRITE MAIN IN SAME CLASS ? OUTER REFERENCE ON INNER OBJECT IS NOT REQUIRED HERE? 

class Outer{

	class Inner{

		void m1(){

			System.out.println("IN M1 INNER ");

		}

	}
	void m2(){

		System.out.println("IN M2 OUTER");

	}
	public static void main(String[] args){

	  	Outer obj = new Outer();
		obj.m2();

		//THIS BELOW LINE WILL GIV ERROR SO THE CONCLUSION IS EVERYTIME WE HAVE TO USE OUTER REFERENCE WHILE CREATING INNER CLASS OBJECT
		// TO RESOLVE THIS ERROR USE :
		//Inner obj1 = new Outer().new Inner();

		Inner obj1 = new Inner();
		obj1.m1();

	}
}

