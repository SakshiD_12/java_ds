

class Outer{

	void m1(){

		System.out.println("IN M1 OUTER");

	}
	static class Inner{

		void m2(){

			System.out.println("IN M2 INNER");

		}

	}
}
class Client{

	public static void main(String[] args){

		Outer obj1 = new Outer();
		obj1.m1();

		Outer.Inner obj = new Outer.Inner();
		obj.m2();

	}
}
