

class Outer{

	static class Inner{

		void m2(){

			System.out.println("IN M2 INNER");

		}

	}
	public static void main(String[] args){
		
		//HERE AS INNER CLASS IS STATIC WE DONT NEED TO MAKE THE OUTER OBJECT TO ACCESS INNER CLASS METHOD 

		Inner obj = new Inner();
		obj.m2();

	}
}
