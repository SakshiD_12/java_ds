
//Anonymous inner class is class with no name 

class Demo{

	Demo(){

		System.out.println("IN DEMO CONSTRUCTOR");

	}

}
class Client{

	public static void main(String[] args){


		//BELOW IS THE ANONYMOUS INNER CLASS 
		Demo obj = new Demo(){



		};
	}
}
