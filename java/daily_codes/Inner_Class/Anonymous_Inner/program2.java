

// NOTE :OBJECT CLASS IS NOT AN PARENT OF ANONYMOUS CLASS INSTRAD THE REFERENCE WHICH HAS GIVEN IS THE PARENT OF ANONYMOUS CLASS

//NOTE : ANONYMOUS CLASS IS ONLY FOR THE ONE TIME USE 

class Demo{

	int x=10;
	void marry(){

		System.out.println("KRITI SANON");

	}
}
class Client{

	public static void main(String[] args){

		//INTERNALLY BELOW LINE GOES AS : Demo obj = new Client$1();
		//                                 (parent)      (child)

		Demo obj = new Demo(){

			void marry(){

				System.out.println("PRATIK SHINDE");

			}
		};
		obj.marry(); //this will call childs marry method i.e clients marry method 

	}
}
