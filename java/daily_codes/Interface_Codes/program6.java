

//NOTE : WE CANNOT INHERIT THE STATIC METHOD IN THE INTERFACE IN CHILD CLASS ELSE IT WILL GIVE ERROR AS CANNOT FIND SYMBOL

interface Demo{

	static void fun(){

		System.out.println("IN FUN");

	}
}
class DemoChild implements Demo{


}
class Clients{

	public static void main(String[] args){

		DemoChild obj = new DemoChild();
		obj.fun();

	}
}
