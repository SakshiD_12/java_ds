

//upto 1.7 we are not allowed to give body for the interface but from java 8 we can give body to the interface 
//but for that we have to declare the method either default or static 

interface Demo{

	void fun();
	default void gun(){

		System.out.println("IN GUN");

	}
}
class Child implements Demo{

	public void fun(){

		System.out.println("IN FUN");

	}
}
class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.fun();
		obj.gun();

	}
}
