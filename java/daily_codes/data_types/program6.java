
class Demo{

	int x=10; //**********INSTANCE VARIABLE************

	public static void main(String[] args){

		int y=18; //**********LOCAL VARIABLE************

		System.out.println(x); 
		System.out.println(y);

	}
}
//NOTE : 
//AS MAIN IS A STATIC FUNCTION IT WILL ONLY ACCESS THE STATIC DATA AND METHHOD WHICH HAS A OBJECT  HENCE
//IT WILL NOT ABLE TO ACCESS THE VARIABLE X AS IT IS NON STATIC
