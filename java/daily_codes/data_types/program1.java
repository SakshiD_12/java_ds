
class Demo{

	public static void main(String[] args){

		byte var1=18;
		byte var2=18;

		System.out.println(var1); //18
		System.out.println(var2); //18

		var1 = var1 + var2; //---->INCOMPATIBLE TYPES(AT RUNTIME JAVA COMPILER RETURN 36 AS INTEGER
		                   //AND WE CANNOT STORE INT INTO BYTE 
		
		System.out.println(var1); 
		System.out.println(var2);

	}
}
