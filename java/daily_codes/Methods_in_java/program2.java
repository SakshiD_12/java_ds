

//NOTE : WE CANNOT CALL NON-STATIC METHOD DIRECTLY FROM THE MAIN THERR ARE TWO WAYS TO CALL NON-STATIC METHOD :
//-------- 1)MAKE A NON-STATIC METHOS STATIC BY WRITING STATIC KEYWORD IN FRONT OF THE METHOD NAME (BUT THIS IS NOT RECOMMENDED METHOS)
//         AS IN JAVA WE CANNOT MAKE EVERYTHING STATIC AS SOME METHOD NEED TO BE NON STATIC 

//         2)SECOND WAY IS TO MAKE OBJECT OF CLASS IN MAIN AND CALL THE METHOS BY USING OBJECT

class Demo{


	static void fun(){

		System.out.println("IN STATIC FUN...........");

	}

	void gun(){

		System.out.println("IN NON STATIC GUN.......");

	}

	public static void main(String[] args){
		
		System.out.println("***************IN MAIN**************");

		Demo obj = new Demo(); // OBJECT OF THE CLASS

		fun();
		obj.gun();

	}
}



