

//METHODS IS FUNCTION ONLY BUT IN JAVA WE SAY METHOD BECUZ JAVA IS PURE OOP LANGUAGE HENCE EVERYTHING IS WRIITEN ISIDE CLASS HENCE EVERYTHING
//INSIDE CLASS CINSIDERED AS METHODS HENCE IN JAVA IT IS METHOD AND NOT FUNCTION

//IN JAVA THERE ARE TWO TYPES OF METHODS -------------> 1) Static methods
//                                                      2)Non-static methods / also called as instance


//NOTE : WE CANNOT CALL NONN-STATIC METHOD DIRECTLY FROM THE MAIN IT WILL GIVE ERROR......
class Demo{

	static void fun(){

		System.out.println("IN FUN.................");

	}

	void gun(){

		System.out.println("IN GUN..................");

	}
	public static void main(String[] args){

		fun();
		gun(); //ERROR : NON-STATIC METHOD gun() CANNOT BE CALLED FROM STATIC CONTENT

	}
}



