
//CHECK WHTHER THE NUMBER IS ARMSTRONG OR NOT

//ARMSTRONG NUMBER IS A NUMBER WHICH EACH DIGIT HAS POWER EQUAL TO THE TOTAL NUMBER OF DIGIT AND THEIR SUM IS EQUAL TO THAT NUMBER ONLY
//EXAMPLE---------> 23--> 2 digit hence power of 2 and their sum--->2^2 + 3^2 = 4 + 9 = 13 .........hence 23 is not armstrong number
//153------> 3 digit hence power of 3 and their sum-------->1^3 + 5^3 + 3^3 = 1+125+27=153.......hence 153 is armstrong number


class Number{

	public static void main(String[] args){

		int N=1634;
		int temp1=N; //BECAUSE N BECOME 0 IN FIRST WHILE LOOP 
		int temp2=N; //BECAUSE N BECOME 0 IN SECOND FOR LOOP
		int count=0; //TO COUNT THE NUMBER OF DIGITS
		int sum=0;   

		//FIRST WE COUNT THE NUMBER OF DIGIT IN THE NUMBER SO WE KNOW THE POWER
		while(temp1 != 0){

			count++;
			temp1=temp1/10;

		}
		//NOW WE MULTIPLY THAT NUMBER UPTO COUNT USING FOR LOOP
		while(N != 0){

			int rem=N%10;
			int mult=1;
			for(int i=1;i<=count;i++){

				mult=mult*rem;

			}
			sum=sum+mult;
			N=N/10;

		}
		if(temp2==sum){

			System.out.println(temp2+" IS ARMSTRONG NUMBER..........");

		}else{

			System.out.println(temp2+" IS NOT ARMSTRONG NUMBER..........");

		}
	}
}

	

