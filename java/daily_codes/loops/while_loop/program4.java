

//PRINT ODD INTEGER FROM 1 TO 10 USING WHILE LOOP and if statment


class Number{

	public static void main(String[] args){

		int i=1;
		int n=20;
		while(i<=n){
			if(i%2!=0){

				System.out.println(i);
			}
			i++;

		}
	}
}
