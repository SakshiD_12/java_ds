
//PRINT PRODUCT OF DIGITS PRESENT IN NUMBER 


class Number{

	public static void main(String[] args){

		int N=1234;

		int mult=1;

		while( N != 0){

			int prod= N%10;

			mult=mult * prod;

			N=N/10;

		}

		System.out.println(mult);

	}
}
