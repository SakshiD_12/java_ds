/*
 
   A B C
   D E F
   G H I

 */

class Number{

	public static void main(String[] args){

		int count=0;
		int row=3;
		char ch=65;
		for(int i=1;i<=row;i++){

			System.out.print((char)(ch)+" ");
			ch++;

		if(i==row){
			
			count++;
			System.out.println();
			i=0;
		
		}

		if(count==row){

			break;

		}
		}
	}
}
