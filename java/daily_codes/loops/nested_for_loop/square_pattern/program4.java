/*
 
   1 2 3 4
   2 3 4 5
   3 4 5 6
   4 5 6 7

 */

class Number{

	public static void main(String[] args){

		int count=0;
		int row=4;
		int x=1;
		for(int i=1;i<=row;i++){

			System.out.print(x+" ");
			x++;

		if(i==row){
			
			count++;
			System.out.println();
			i=0;
			x=x-3;
		}

		if(count==row){

			break;

		}
		}
	}
}
