/*
 
   A 1 B 2
   A 1 B 2
   A 1 B 2
   A 1 B 2

*/

class Number{

	public static void main(String[] args){

		int count=0;
		int row=4;
		int x=1;
		char ch=65;
		for(int i=1;i<=row;i++){
			
			if(i%2==0){

			  	System.out.print(x+" ");
				x++;

			}else{
				System.out.print((char)(ch) + " ");
				ch++;

			}
		if(i==row){
			
			count++;
			System.out.println();
			i=0;
			x=1;
			ch=65;
		
		}

		if(count==row){

			break;

		}
		}
	}
}
