/*
 
 A 1 B 2
 C 3 D
 E 4
 F
 
 */

class Number{

	public static void main(String[] args){

		int N=4;
		int x=1;
		char ch=65;
		for(int i=1;i<=N;i++){

			for(int j=N;j>=i;j--){
				
				if(j%2==1){

				  	System.out.print(x+" ");
					x++;

				}else{
					System.out.print((char)(ch)+" ");
					ch++;

				}
			}
			System.out.println();

		}
	}
}
