
//Continue : 1)CONTINUE SKIPS THE PARTICULAR VALUE INSTEAD IF BREAKING THE WHOLE LOOP.
//           2)COINTINUE ONLY WORKS ON LOOP.


class Number{

	public static void main(String[] args){

		int n=40;
		for(int i=1;i<=n;i++){

			if((i%3==0 && i%5==0) || i%4==0){

				continue;

			}
			System.out.println(i);

		}
	}
}
