

//NOTE------> WE CAN WRITE DEFAULT AT THE TOP ALSO BUT IT HAS LEAST PRIORITY THAN THE OTHERS


class SwitchDemo{

	public static void main(String args[]){

		int x=2;

		switch(x){

			default:
				System.out.println("WRONG CHOICE");
		               // break;

			case 1:
				System.out.println("ONE");
				
		                break;
			case 2:
				System.out.println("TWO");
		                break;
				
			case 3:
				System.out.println("THREE");
		                break;
				
			case 4:
				System.out.println("FOUR");
		                break;
				
			case 5:
				System.out.println("FIVE");
		                break;
				
		}
	}
}




