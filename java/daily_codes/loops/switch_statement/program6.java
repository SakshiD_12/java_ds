

//NOTE------> IN JAVA CASE LABEL MUST BE ANY CONSTANT VALUE AND NOT AN VARIABLE OR EXPRESSION OR ANYTHING ELSE WITHOUT CONSTANT 


class SwitchDemo{

	public static void main(String args[]){

		int x=3;
		int a=1;
		int b=2;

		switch(x){

			case a: //-------------------------------->THIS A WILL INTERNALLY GOES AS 65
				System.out.println("CHAR--A");
				
		                break;
			case b:  //---------------------------------->DUPLICATE CASE LABEL
				System.out.println("NUM--65");
		                break;
				
			case a+b:
				System.out.println("CHAR--B");
		                break;
				
			case a+a+b:  //---------------------------------->DUPLICATE CASE LABEL
				System.out.println("NUM--66");
		                break;
				
			case 5:
				System.out.println("FIVE");
		                break;
				
			default:
				System.out.println("WRONG CHOICE");
		                break;
		}
		System.out.println("AFTER SWITCH");
	}
}




