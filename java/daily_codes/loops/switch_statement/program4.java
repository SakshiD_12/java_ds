

//NOTE------> IF WE WRITE TWO SAME CASES THE IT WILL GIVE ERROR AS DUPLICATE CASE LABEL FOR THE DUPLICATE ONE LINE.
//----------->IN SWITCH IN JAVA IT WILL EVALUATE ARITHMETIC EXPRESSION  IN CASE LABEL 


class SwitchDemo{

	public static void main(String args[]){

		int x=5;

		switch(x){

			case 1:
				System.out.println("ONE");
				
		                break;
			case 2:
				System.out.println("TWO");
		                break;
				
			case 1+1:
				System.out.println("THREE"); //ERROR-------->DUPLICATE CASE LABEL (IT EVALUATES THE ARITHMRTIC EXPRESSION)
		                break;
				
			case 3:
				System.out.println("FOUR");
		                break;
				
			case 1+2:
				System.out.println("FIVE"); //ERROR----->DUPLICATE CASE LABEL (IT EVALUATES)
		                break;

			default:
				System.out.println("WRONG CHOICE");
		                break;
		}
		System.out.println("AFTER SWITCH");
	}
}




