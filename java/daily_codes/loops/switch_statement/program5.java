

//NOTE------> IF WE WRITE TWO SAME CASES THE IT WILL GIVE ERROR AS DUPLICATE CASE LABEL FOR THE DUPLICATE ONE LINE.
//
//-------->IN JAVA CHARACTER LABEL INTERNALLY GOES AS ASCII VALUE OF THAT CHARACTER


class SwitchDemo{

	public static void main(String args[]){

		int ch=65;

		switch(ch){

			case A: //-------------------------------->THIS A WILL INTERNALLY GOES AS 65
				System.out.println("CHAR--A");
				
		                break;
			case 65:  //---------------------------------->DUPLICATE CASE LABEL
				System.out.println("NUM--65");
		                break;
				
			case B:
				System.out.println("CHAR--B");
		                break;
				
			case 66:  //---------------------------------->DUPLICATE CASE LABEL
				System.out.println("NUM--66");
		                break;
				
			case 5:
				System.out.println("FIVE");
		                break;
				
			default:
				System.out.println("WRONG CHOICE");
		                break;
		}
		System.out.println("AFTER SWITCH");
	}
}




