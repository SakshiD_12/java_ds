

//NOTE------> IF WE WRITE TWO SAME CASES THE IT WILL GIVE ERROR AS DUPLICATE CASE LABEL FOR THE DUPLICATE ONE LINE.


class SwitchDemo{

	public static void main(String args[]){

		int x=2;

		switch(x){

			case 1:
				System.out.println("ONE");
				
		                break;
			case 2:
				System.out.println("TWO");
		                break;
				
			case 3:
				System.out.println("THREE");
		                break;
				
			case 4:
				System.out.println("FOUR");
		                break;
				
			case 5:
				System.out.println("FIVE");
		                break;
				
			case 5:
				System.out.println("SECOND---FIVE"); //--------->ERROR(DUPLICATE CASE LABEL)
		                break;

			case 2:
				System.out.println("SECOND---TWO"); //--------->ERROR (DUPLICATE CASE LABLE)
		                break;
			default:
				System.out.println("WRONG CHOICE");
		                break;
		}
		System.out.println("AFTER SWITCH");
	}
}




