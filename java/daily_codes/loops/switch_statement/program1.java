

//NOTE------> IF WE DID NOT WRITE THE BREAK AFTER EACH CASE IT WILL RETURN EVERYTHING AFTER IT FIND THE CORRECT CONDITION
//            WHATEVER IS WRITTEN IN DEFALT IS ALSO RETURNED AS IS CONSIDER DEFAULT AS A NORMAL CASE LIKE OTHERS.

class SwitchDemo{

	public static void main(String args[]){

		int x=2;

		switch(x){

			case 1:
				System.out.println("ONE");
				
			case 2:
				System.out.println("TWO");
				
			case 3:
				System.out.println("THREE");
				
			case 4:
				System.out.println("FOUR");
				
			case 5:
				System.out.println("FIVE");
				
			default:
				System.out.println("WRONG CHOICE");
		}
	}
}




