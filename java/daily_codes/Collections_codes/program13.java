
//******************************** ENUMERATION *************************************

import java.util.*;
class CursorDemo{
	public static void main(String[] args){

		Vector al = new Vector();

		al.add("ASHISH");
		al.add("KANHA");
		al.add("RAHUL");
		al.add("BADHE");

		System.out.println(al); 

		Enumeration itr = al.elements();

		while(itr.hasMoreElements()){
			System.out.println(itr.nextElement());
		}
	}
}
