
//************************************************** LIST *********************************************************

//list interface works on index also
//list can store duplicate data
//and also preserve the insertion order
// 4 classes of list interface are 1) ArrayList class 2)LinkedList class  3)vector class 4)Stack class(child of vector)

//********************************************* ArrayList Class ****************************************************

//SIZE OF ELEMENT IN ARRAY LIST IS NOT FIXED IT IS DYNAMIC IN SIZE 
//WE CAN ADD ANY TYPE OF DATA NOT ONLY SIMILAR TYPE OF DATA

import java.util.*;

class ArrayListDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20.5f);
		al.add("SAKSHI");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);

	}
}
