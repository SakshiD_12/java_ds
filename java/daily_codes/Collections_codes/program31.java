

//************************************* SortedMap Interface ******************************************


import java.util.*;
class TreeMapDemo{

	public static void main(String[] args){

		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");
		tm.put("Sl","SriLanka");

		System.out.println(tm);

		//METHODS OF SORTEDMAP

		//1)subMap()
		System.out.println(tm.subMap("Aus","Pak"));

		//2)headMap()
		System.out.println(tm.headMap("Pak"));

		//3)tailMap()
		System.out.println(tm.tailMap("Pak"));

		//4)firstKey()
		System.out.println(tm.firstKey());

		//5)lastKey()
		System.out.println(tm.lastKey());

		//6)keySet()
		System.out.println(tm.keySet());

		//7)values()
		System.out.println(tm.values());

		//8)entrySet()
		System.out.println(tm.entrySet());

	}
}
