
//************************** BLOCKING QUEUE ************************************

import java.util.concurrent.*;
import java.util.*;

class Demo{
	public static void main(String[] args) throws InterruptedException{

		BlockingQueue bq = new ArrayBlockingQueue(3);

		bq.offer(10);
		bq.offer(10);
		bq.offer(10);
		
		System.out.println(bq);

		bq.put(40);
		System.out.println(bq);
	}
}
