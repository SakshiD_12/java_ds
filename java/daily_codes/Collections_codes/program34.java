
//*********************************************** Dictionary CLASS ***************************************************

import java.util.*;
class HashtableDemo{
	public static void main(String[] args){

		Dictionary ht = new Hashtable();

		ht.put(10,"Sachin");
		ht.put(7,"Dhoni");
		ht.put(18,"Virat");
		ht.put(1,"KLRahul");
		ht.put(45,"Rohit");

		System.out.println(ht);

		//METHODS OF DICTIONARY CLASS :

		//1) Enumeration keys()

		Enumeration itr1 = ht.keys(); //it will iterate and print only keys 

		while(itr1.hasMoreElements()){

			System.out.println(itr1.nextElement());

		}

		//2) Enumeration elements() :

		Enumeration itr2 = ht.elements(); //it will iterate and print only values

		while(itr2.hasMoreElements()){

			System.out.println(itr2.nextElement());

		}

		//3)get(key):
		System.out.println(ht.get(10)); //returns value of given key

		//4)remove():
		ht.remove(1); //remove value of given key

		System.out.println(ht);



	}
}
