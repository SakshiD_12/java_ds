
//**************** PROGRAM TO ADD USER DEFINED CLASS IN COLLECTION *********************
import java.util.*;

class CricPlayer{

	int jerNo = 0;
	String Name  = null;

	CricPlayer(int jerNo ,  String Name){

		this.jerNo = jerNo;
		this.Name = Name;

	}

	//TO PRINT THE DATA IN USER DEFINED CLASS WE HAVE TO OVERRIDE THE toString() METHOD OF OBJECT CLASS 
	public String toString(){

		return jerNo+" : "+Name;

	}
}
class ArrayListDemo{
	public static void main(String[] args){

		ArrayList obj = new ArrayList();

		obj.add(new CricPlayer(18,"VIRAT"));
		obj.add(new CricPlayer(07,"DHONI"));
		obj.add(new CricPlayer(45,"ROHIT"));

		//HERE BELOW LINE IS NOTHING BUT obj.add(new Integer(10)); BECAUSE IN COLLECTION EVRYTHING IS OBJECT HENCE PREDEFINED DATA CANNOT BE CONSIDERED IN COLLECTION IT IS CONVERT INTO CLASS USING WRAPPER CLASS 
		//Integer CLASS COMES UNDER OBJECT CLASS HENCE OBJECT CLASS METHOD AUTOMATICALLY COME TO PREDEFINED DATA WE DONT NEED TO WRITE toString() method here
		obj.add(10);
		obj.add(new Integer(20));

		System.out.println(obj);

	}
}
