

//**********************************  METHODS OF ArrayList Class ************************************

import java.util.*;

class ArrayListDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20.5f);
		al.add("SAKSHI");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);

		// 1) int size() : RETURNS THE SIZE 
		System.out.println(al.size()); //5

		// 2) boolean contains(object) : CHECKS GIVEN VALUE PRESENT IN LIST OR NOT AND RETURN TRUE IF PRESENT ELSE FALSE
		System.out.println(al.contains("SAKSHI")); //true
		System.out.println(al.contains(30));   //false

		// 3) int indexOf(object) : RETURNS THE INDEX OF VALUE GIVEN 
		System.out.println(al.indexOf("SAKSHI")); //2

		// 4) int lastIndexOf(object) : RETURNS THE LAST OCCURRENCE OF GIVE VALUE  
		System.out.println(al.lastIndexOf(20.5f)); //4

		// 5) E get(int) : AS RETURN TYPE IS ELEMENT IT RETURNS THE VALUE AT SPECIFIED INDEX 
		System.out.println(al.get(2)); //SAKSHI

		// 6) E set(int , E) : SET THE VALUE AT GIVEN INDEX AND AS RETURN TYPE IS ELEMENT IT RETURNS THE REPLACED VALUE 
		System.out.println(al.set(3,"SAYALI")); //10
		System.out.println(al);

		// 7) void add (int, E) : ADD GIVEN ELEMENT AT GIVEN INDEX (NOT REPLACE )
		al.add(3,10);
		System.out.println(al);

		// 8) boolean addAll(collection) : ADD A NEW WHOLE COLLECTION TO EXISTING COLLECTION 

		ArrayList al2 = new ArrayList();

		al2.add("AI");
		al2.add("ML");
		al2.add("DSA");

		al.addAll(al2);
		System.out.println(al);

		// 9) boolean addAll(int , collection) : ADD A COLLECTION FROM SPECIFIED INDEX 

		al.addAll(3,al2);
		System.out.println(al);

		// 10) java.lang.object [] toArray(); : CONVERT COLLECTION TO ARRAY 

		Object arr[]=al.toArray();
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+ " ");

		}
		System.out.println();

		// 11) E remove(object) : remove value at given index and return it 
		System.out.println(al.remove(4));

		// 12) boolean remove(object) : remove value at given index return true if removed successfully
		System.out.println(al.remove("DSA")); //true
		System.out.println(al.remove("cpp")); //false
		System.out.println(al);

		// 13) void clear() : clear whole list
		al.clear();
		System.out.println(al); //[]
	}
}
