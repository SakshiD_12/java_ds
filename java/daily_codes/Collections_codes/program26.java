
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//1) IdentityHashMap Class :
//duplicate keys allowed in IdentityHashMap

import java.util.*;
class HashDemo{
	public static void main(String[] args){

		IdentityHashMap hm = new IdentityHashMap();

		hm.put(new Integer(10),"A");
		hm.put(new Integer(10),"B");
		hm.put(new Integer(10),"C");
		hm.put(new Integer(10),"D");
		

		System.out.println(hm);

	}
}
