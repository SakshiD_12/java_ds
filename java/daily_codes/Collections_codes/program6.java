

//*************************************************************** CURSOR **************************************************************************

//THERE ARE 4 TYPES OF CURSORS : 1)Iterator 2)ListIterator 3)Spliterator 4)Enumeration 

//*************************************************** ITERATOR ************************************************************

//1)its alwyas recommend to not to use for loop and for each loop on collection hence cursors are used to travers through collection
//2)ITERAOR IS ONE TYPE OF INTERFACE IT HAS 4 METHODS 
//                                                  1) public abstract boolean hasNext(); 
//                                                  2) public abstract E next(); 
//                                                  3) public default void remove();
//                                                  4) public default void ForEachRemoving();
import java.util.*;
class IteratorDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add("KANHA");
		al.add("RAHUL");
		al.add("ASHISH");

		Iterator itr = al.iterator();

		while(itr.hasNext()){ //has next only checks the value is present or not in next block 
			
			System.out.println(itr.next()); //next ek ghar pudhe sarkto 

		}
	}
}
