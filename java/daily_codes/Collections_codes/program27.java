
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//1) WeakHashMap Class :
//this comes in frame to remove the drawback of hashmap that is in hashmap even if we make a object null it will not allow to garbage collector to 
//remove that unreferenced object from the heap 
//hashmap doinate the garbage collector 
//thats why weakhashp map comes in frame

import java.util.*;
class Demo{

	String str;

	Demo(String str){

		this.str=str;

	}
	public String toString(){

		return str;

	}
	public void finalize(){

		System.out.println("NOTIFY");

	}
}
class HashDemo{
	public static void main(String[] args){

		Demo obj1=new Demo("C2W");
		Demo obj2=new Demo("C3W");
		Demo obj3=new Demo("C4W");


		WeakHashMap hm = new WeakHashMap();

		hm.put(obj1,"INFOSYS");
		hm.put(obj2,"BARCLAYS");
		hm.put(obj3,"CarPro");
		
		obj1=null;
		obj2=null;

		System.gc(); //THIS WILL FORCEFULLY CALL THE FINALIZE METHOD OF THE OBJECT THAT IS NULL AND THEN FREE THE MEMORY AFTER THAT 

		System.out.println(hm);

	}
}
