
//**************************************** METHODS OF QUEUE *************************************************


import java.util.*;
class QueueDemo{

	public static void main(String[] args){

		Queue obj = new LinkedList();

		obj.offer(10);
		obj.offer(20);
		obj.offer(50);
		obj.offer(30);
		obj.offer(40);

		System.out.println(obj);

		obj.poll();
		System.out.println(obj);

		obj.remove();
		System.out.println(obj);

		//obj.peek();
		System.out.println(obj.peek());

		//obj.element();
		System.out.println(obj.element());



	}
}


