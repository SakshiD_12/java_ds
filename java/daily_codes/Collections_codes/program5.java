
//********************************************* LinkedList Class Methods ******************************************************************

import java.util.*;

class LinkedListDemo{
	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.add(20);

		ll.addFirst(30);

		ll.addLast(50);

		ll.add(2,10);

		System.out.println(ll); // 30 10 10 20 50

		System.out.println(ll.get(3)); //20

		System.out.println(ll.getFirst()); //30

		System.out.println(ll.getLast()); //50

		System.out.println(ll.removeFirst()); //30

		System.out.println(ll.removeLast()); //50
	}
}
