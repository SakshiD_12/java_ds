
//******************************** LIST ITERATOR *************************************

import java.util.*;
class CursorDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add("ASHISH");
		al.add("KANHA");
		al.add("RAHUL");
		al.add("BADHE");

		System.out.println(al); 

		ListIterator itr = al.listIterator();

		while(itr.hasNext()){
			
			System.out.println(itr.next());

		}
		while(itr.hasPrevious()){

			System.out.println(itr.previous());

		}

	}
}
