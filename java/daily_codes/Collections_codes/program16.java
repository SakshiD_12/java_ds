
//********************************* LinkedHashedSet Class(USER-DEFINED) **********************************************

import java.util.*;
class cricPlayer implements Comparable{

	String name=null;
	int jerNo=0;

	cricPlayer(String name,int jerNo){

		this.name=name;
		this.jerNo=jerNo;

	}
	public int compareTo(Object obj){

		return name.compareTo(((cricPlayer)obj).name);

	}
	public String toString(){

		return name;

	}
}
class LinkedHashedSetDemo{
	public static void main(String[] args){

		LinkedHashSet hs = new LinkedHashSet();


		hs.add(new cricPlayer("DHONI",07));
		hs.add(new cricPlayer("VIRAT",18));
		hs.add(new cricPlayer("DHONI",07));
		hs.add(new cricPlayer("ROHIT",45));
			
		System.out.println(hs);

	}
}
