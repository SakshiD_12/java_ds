
//*************************************************** TreeSet Class ****************************************

//Tree set sort the data 
//tree set doen not have comparable interface 
//add method of tree set internally called the compareTo method 
//Tree set can take data that can be comparable in nature 
//when we write the user defined data implements comparable and overried compareTo method 

import java.util.*;
class TreeSetDemo{
	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add("KANHA");
		ts.add("ASHISH");
		ts.add("RAHUL");
		ts.add("BADHE");
		ts.add("SHASHI");

		System.out.println(ts);

	}
}

