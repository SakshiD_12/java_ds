
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//1) HashMap Class 

//KEY SHOULD BE UNIQUE AND IF WE GIVE DUPLICATE KEY IT WILL REPLACE OLD KEY WITH NEW KEY 

//PROOF :

import java.util.*;
class HashDemo{
	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Kanha","INFOSYS");
		hm.put("Ashish","BARCLAYS");
		hm.put("Badhe","CarPro");
		hm.put("Kanha","BMC");

		System.out.println(hm);

	}
}
