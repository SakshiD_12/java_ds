

//************************************* ITERATING OVER MAP  ******************************************

//CURSOR WILL NOT WORK ON MAPS HENCE FIRST WE HAVE TO CONVERT MAP INTO SET USING entrySet() .

import java.util.*;
class TreeMapDemo{

	public static void main(String[] args){

		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");
		tm.put("Sl","SriLanka");

		System.out.println(tm);

		//CONVERTING INTO SET :
		Set<Map.Entry> data = tm.entrySet();

		//NOW SET ITERATOR IN THAT SET :
		Iterator<Map.Entry> itr = data.iterator();

		//PRINTING VALUES IN SET USING ITERATOR :
		while(itr.hasNext()){

			//WHEN WE WANR TO PRINT ALL THE KEYS AND VALUES PAIR USE :
			//System.out.println(itr.next);

			//BUT WHEN WE WANT TO PRINT THE CUSTOMISED O/P LIKE ONLY KEYS AND ONLY VALUES USE getKey() getValues() first by storing key values in another variable :
			Map.Entry abc = itr.next();
			System.out.println(abc.getKey()+":"+abc.getValue());

		}
		
	}
}
