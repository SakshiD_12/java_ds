

//************************************* TREE MAP CLASS ******************************************

//TREEMAP GIVES SORTED OUTPUT
import java.util.*;
class TreeMapDemo{

	public static void main(String[] args){

		TreeMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");
		tm.put("Sl","SriLanka");

		System.out.println(tm);
	}
}
