
//******************************** ITERATOR *************************************

import java.util.*;
class CursorDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add("ASHISH");
		al.add("KANHA");
		al.add("RAHUL");
		al.add("BADHE");

		for (var x :al){

			System.out.println(x.getClass().getName());

		}

		System.out.println(al); 

		Iterator itr = al.iterator();

		while(itr.hasNext()){
			if("KANHA".equals(itr.next())){
				itr.remove();
			}
		}
		System.out.println(al);
	}
}
