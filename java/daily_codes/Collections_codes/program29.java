

//************************************* TREE MAP CLASS ******************************************

//REAL TIME EXAMPLE OF OF TREE MAP USER DEFINED DATA(USING COMPARABLE)
import java.util.*;

class Platform implements Comparable{

	String str=null;
	int year=0;

	Platform(String str ,int year){

		this.str=str;
		this.year=year;

	}
	public String toString(){

		return "{"+str+":"+year+"}";

	}
	public int compareTo(Object obj){

		return this.year - ((Platform)obj).year;

	}
}
class TreeMapDemo{

	public static void main(String[] args){

		TreeMap tm = new TreeMap();

		tm.put(new Platform("Ind",2005),"India");
		tm.put(new Platform("Pak",2013),"Pakistan");
		tm.put(new Platform("Aus",2004),"Australia");
		tm.put(new Platform("Ban",2022),"Bangladesh");
		tm.put(new Platform("Sl",2029),"SriLanka");

		System.out.println(tm);
	}
}
