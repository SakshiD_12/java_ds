
import java.util.*;
class PropertiesDemo{
	public static void main(String[] args) throws IOException{


		Properties obj = new Properties();

		//ToRead :
		FileInputStream fobj = new FileInputStream("friends.properties");
		 
		obj.load(fobj);

		String name = obj.getProperty("Ashish");
		System.out.println(name);

		obj.setProperty("Sakshi","Google");

		FileOutputStream outobj = new FileOutputStream("friends.properties");
		obj.store(outobj,"UPDATED BY SAKSHI");

	}
}
