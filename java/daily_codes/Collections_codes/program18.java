
//*************************************************** TreeSet Class ***************************************

//Tree set only take data that can be comparable in nature  : PROOF ------------------->

import java.util.*;
class TreeSetDemo{
	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		//STRING BUFFER IS COMPARABLE IN NATURE 

		ts.add(new StringBuffer("KANHA"));
		ts.add(new StringBuffer("ASHISH"));
		ts.add(new StringBuffer("RAHUL"));
		ts.add(new StringBuffer("BADHE"));
		ts.add(new StringBuffer("SHASHI"));

		System.out.println(ts);

	}
}

