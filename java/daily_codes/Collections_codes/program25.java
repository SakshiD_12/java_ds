
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//******************************************* METHODS OF HashMap Class **********************************************************

import java.util.*;
class HashDemo{
	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Kanha","INFOSYS");
		hm.put("Ashish","BARCLAYS");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");

		System.out.println(hm);

		//1)get():RETURNS VALUE OF GIVEN KEY 
		System.out.println(hm.get("Badhe"));

		//2)keySet():RETURNS ALL THE KEYS 
		System.out.println(hm.keySet());

		//3)values():RETURNS ALL THE VALUES  
		System.out.println(hm.values());

		//4)entrySet():CONVERT MAP TO SET 
		System.out.println(hm.entrySet());


	}
}
