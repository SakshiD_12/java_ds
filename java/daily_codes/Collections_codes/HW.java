
// ***************************** REALTIME EXAMPLE ***************************************

import java.util.*;

class Languages{

	String Lname = null;
	int year = 0;

	Languages(String Lname  , int year){

		this.Lname=Lname;
		this.year=year;

	}
	public String toString(){

		return "{"+Lname+":"+year+"}";

	}
}
class SortByYear implements Comparator{
	
	public int compare(Object obj1,Object obj2){

		return (int)((((Languages)obj1).year) -  (((Languages)obj2).year));

	}
}
class ListSortDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(new Languages("JAVA",1995));
		al.add(new Languages("DART",2013));
		al.add(new Languages("CPP",1985));
		al.add(new Languages("PYTHON",1991));
		al.add(new Languages("C",1972));

		Collections.sort(al,new SortByYear());
		System.out.println(al);

	}
}
