
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//1) LinkedHashMap Class 

//THIS WILL PRESERVES THE INSERTION ORDER 

import java.util.*;
class HashDemo{
	public static void main(String[] args){

		LinkedHashMap hm = new LinkedHashMap();

		hm.put("Kanha","INFOSYS");
		hm.put("Ashish","BARCLAYS");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");

		System.out.println(hm);

	}
}
