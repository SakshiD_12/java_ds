

//************************************* TREE MAP CLASS ******************************************

//REAL TIME EXAMPLE OF OF TREE MAP USER DEFINED DATA(USING COMPARATOR)
import java.util.*;

class Platform{

	String str=null;
	int year=0;

	Platform(String str ,int year){

		this.str=str;
		this.year=year;

	}
	public String toString(){

		return "{"+str+":"+year+"}";

	}
}

class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){

		return ((Platform)obj1).str.compareTo(((Platform)obj2).str);

	}
}
class TreeMapDemo{

	public static void main(String[] args){

		TreeMap tm = new TreeMap(new SortByName()); //here we have to pass this because treemap doesnt have 
		//collections.sort() method with map parameter but treemap have constructor with comparater parameter

		tm.put(new Platform("Ind",2005),"India");
		tm.put(new Platform("Pak",2013),"Pakistan");
		tm.put(new Platform("Aus",2004),"Australia");
		tm.put(new Platform("Ban",2022),"Bangladesh");
		tm.put(new Platform("Sl",2029),"SriLanka");

		System.out.println(tm);
	}
}
