//********************************************** NavigableSet Interface ***************************************************************
import java.util.*;
class SortedDemo{
	public static void main(String[] args){

		NavigableSet ns = new TreeSet();

		ns.add(10);
		ns.add(25);
		ns.add(15);
		ns.add(8);
		ns.add(30);
		System.out.println(ns);

		//1)celling() : returns least element in the set which is greater or equal to given element

		System.out.println(ns.ceiling(20)); //25

		//2)floor() : returns the greatest element in the set which is less than or equal to given element 

		System.out.println(ns.floor(20)); //15

		//3)lower() : returns the greatest element but from the set which is less than given element
		
		System.out.println(ns.lower(12)); //10

		//4)higher() : returns the least element but from the set which is greater than given element
		
		System.out.println(ns.higher(12)); //15

		//5)pollFirst() : removes the first lowest element in set

		System.out.println(ns.pollFirst()); //8

		//6)pollLast() : removes the last highest element in set

		System.out.println(ns.pollLast()); //30
	}
}

