
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//1) HashMap Class 

//HashSet internally calls HashMap's put() method thats wht ouput of HashSet and HashMap is Same 

//proof :

import java.util.*;
class HashDemo{

	public static void main(String[] args){

		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");

		System.out.println(hs);

		HashMap hm = new HashMap();

		hm.put("Kanha","INFOSYS");
		hm.put("Ashish","BARCLAYS");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");

		System.out.println(hm);

	}
}
