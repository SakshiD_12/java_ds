
//******************************************************* MAP INTERFACE ***********************************************************

//MAP IS USED WHEN WE WANT TO ADD DATA IN KEY VALUE PAIR
//KEY SHOULD BE UNIQUE AND VALUE CAN BE REPEATED 

//1) HashMap Class 

import java.util.*;
class HashDemo{
	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Kanha","INFOSYS");
		hm.put("Ashish","BARCLAYS");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");

		System.out.println(hm);

	}
}
