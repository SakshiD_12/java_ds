
//********************************************************** VECTOR CLASS ************************************************************************

import java.util.*;

class VectorDemo{
	public static void main(String[] args){

		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v);

		System.out.println(v.capacity()); //10

		System.out.println(v.removeElement(20));  //true
		
		System.out.println(v);

		System.out.println(v.removeElement(2)); //NO ERROR BECAUSE VECTOR COMES UNDER LIST WHICH CAN ACCESS INDEX BUT O/P IS False
		System.out.println(v);
	}
}
