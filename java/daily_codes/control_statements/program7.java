
//print fizz if number is divisible by 3
//print buzz if number is divisible by 5
//print fizz-buzz if number is divisible by both
//if not by both then print not divisible by both


class Number{

	public static void main(String[] args){

		int x=4;

		if(x%3==0 && x%5==0){

			System.out.println(x+ " is divisible by both...........");

		}else if(x%3==0){

			System.out.println(x+ " is only divisible by 3...........");

		}else if(x%5==0){

			System.out.println(x+ " is only divisible by 5...........");

		}else{
			System.out.println(x+ " is not divisible by both...........");

		}
	}
}
