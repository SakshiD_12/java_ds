
// == : DOUBLE EQUAL TO COMPARES THE IDENTITY HASH CODES OF STRINGS AND NOT A CONTENT OF STRINGS

class Demo{

	public static void main(String[] args){

		String str1="SAKSHI"; //SCP
		String str2=new String("SAKSHI"); //HEAP

		if(str1==str2){ //-------> HERE == COMPARES THE HASH CODES OF STR1 AND STR2 

			System.out.println("EQUALS..........");

		}else{
			System.out.println("NOT EQUALS.........");

		}
	}
}
