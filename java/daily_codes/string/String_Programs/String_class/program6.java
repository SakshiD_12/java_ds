
// 1stString.equals(2ndString) : HERE equals() METHOD COMPARES THE CONTENT OF THE STRING 

class Demo{

	public static void main(String[] args){

		String str1="SAKSHI"; //SCP
		String str2=new String("SAKSHI"); //HEAP

		if(str1.equals(str2)){ //-------> HERE CONTENT OF STRING IS BEING CHECKED

			System.out.println("EQUALS..........");

		}else{
			System.out.println("NOT EQUALS.........");

		}
	}
}
