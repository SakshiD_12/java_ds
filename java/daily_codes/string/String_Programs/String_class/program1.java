

//THERE ARE 3 WAYS TO WRITE STRING :

class Demo{

	public static void main(String[] args){

		//-------------------------- METHOD 1 ---------------------------------
		//1)Using character array

		char str1[]={'C','2','W'};

		System.out.println(str1);

		//------------------------ METHOD 2 ------------------------------
		//2)USING STRING CLASS ( without new i.e it will go on ****** STRING CONSTANT POOL ****** )

		String str2="SAKSHI";

		System.out.println(str2);

		//----------------------- METHOD 3 -------------------------------
		//3)USING STRING CLASS (WITH A NEW KEYWORD i/e it will go on ***** HEAP ***** )

		String str3= new String("CORE2WEB");

		System.out.println(str3);

	}
}
