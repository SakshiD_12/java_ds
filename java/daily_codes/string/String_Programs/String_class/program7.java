

//WHAT IS DIFFERENCE BETWEEN + AND CONCAT() AS IT DOES THE SAME THING BUT WHY THIS 2 ARE DIFFERENT 

class Demo{

	public static void main(String[] args){

		String str1="SAKSHI";
		String str2="DHUMAL";


		String str3=str1+str2; // + operator calls append method of string biulder class internally hence it will append at the end 

		String str4=str1.concat(str2); //CONCAT IS THE METHOD OD STRING CLASS IT WILL CONCAT 2 STRINGS (STACK FRAME IS PUSHED OF CONCAT)

		System.out.println(str3);
		System.out.println(str4);

	}
}

