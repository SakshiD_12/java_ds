

class Demo{

	public static void main(String[] args){

		String str1="KANHA";
		String str2=str1;

		String str3 = new String(str2);

		System.out.println(System.identityHashCode(str1)); //SCP
		System.out.println(System.identityHashCode(str2)); //SCP AND HAS SAME ADDRESS AS ABOVE
		System.out.println(System.identityHashCode(str3)); //HEAP
	}
}
