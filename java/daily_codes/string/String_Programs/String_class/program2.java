
//NOTE :
//  1. ON STRING CONSTANT POOL IF WE WRITE TWO SAME STRINGS THEN NO NEW OBJECT WILL CREATE FOR DUPLICATE STRING HASHCODE WILL BE SAME
//
//  2.ON HEAP EVEN IF TWO STRINGS ARE SAME EVERYTIME IF THERE IS NEW KEYWORD IT WILL CREATE NEW OBJECT EVEN IF FOR SAME STRING THERE IS ALREADY A OBJECT PRESENT 

class StringDemo{

	public static void main(String[] args){

		//ON SCP 2 SAME STRINGS HAVE ONE OBJECT ON SCP AND SAME HASH CODE 

		String str1="SAKSHI";
		String str2="SAKSHI";

		System.out.println("ON SCP HASHCODES :");
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		//ON HEAP 2 SAME STRINGS HAVE DIFFERENT OBJECTS
		
	      	
		String str3 = new String("DHUMAL");	
		String str4 = new String("DHUMAL");

		System.out.println("ON HEAP HASHCODES :");
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}

