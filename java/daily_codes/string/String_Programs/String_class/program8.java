

//DIFFERENCE BETWEEN identityHashCode and HashCode

//HASHCODE IS ONE TYPE OF FORMULA THAT COMPARES THE CONTENT OF THE STRING 
class Demo{

	public static void main(String[] args){

		String str1="SAKSHI";
		String str2=new String("SAKSHI");
		String str3="SAKSHI";
		String str4=new String("SAKSHI");

		System.out.println("IDENTITY HASH CODES :");

		//HERE WE GOT STR1 AND STR3 HAS SAME IHC BUT NOT STR2 AND STR3 IHC COMPARES THE LOCATION AND GIVES EVERY OBJECT UNIQUE NUMBER
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		
		System.out.println("HASH CODES :");

		//HERE ALL STRINGS GOT SAME HASH CODE AS HASH CODE COMPARES THE CONTENT OF STRING HENCE SAKSHI IS SAME FOR ALL STRINGS 
		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
