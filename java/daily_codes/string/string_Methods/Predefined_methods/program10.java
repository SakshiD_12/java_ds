//METHOD 10 : lastIndexOf(char ch , int uptoindex);

//FIND THE LAST INDEX IF CHARACTER UPTO THAT INDEX 

class Demo{

	public static void main(String[] args){

		String str1="SAKSHI";

		System.out.println(str1.lastIndexOf("S",0)); //search upto 0 index hence 0
		System.out.println(str1.lastIndexOf("S",2)); //search upto 2nd index 0
		System.out.println(str1.lastIndexOf("S",4)); //3

	}
}

