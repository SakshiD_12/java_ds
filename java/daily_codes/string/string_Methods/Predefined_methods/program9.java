
//METHOD 9 : indexOf(char ch, int fromIndex)

//----------> FIND THE FIRST INDEX OF CHARACTER AND RETURN THE INDEX 
class Demo{

	public static void main(String[] args){

		String str1="SAKSHI";

		System.out.println(str1.indexOf('S',0));  //START SEARCH FROM 0
		System.out.println(str1.indexOf('S',1));  //START SEARCH FROM 1ST INDEX
		System.out.println(str1.indexOf('S',4));

	}
}
