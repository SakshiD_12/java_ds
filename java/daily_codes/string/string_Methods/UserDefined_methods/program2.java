
//METHOD 2 : myLength()

import java.io.*;
class Demo{

	static int myLength(String str1){

		//FIRST WE HAVE TO CONVERT OUR STRING TO ARRAY USING PREDEFINED METHOD toCharArray()

		char arr[] = str1.toCharArray();
		int count=0;
		for(int i=0;i<arr.length;i++){

			count++;
		}
		return count;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER STRING :");
		String str1=br.readLine();

		System.out.println(myLength(str1));
	}
}


