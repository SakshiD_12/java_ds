
//METHOD 3 : myCharAt(int)

import java.io.*;
class Demo{

	static char myCharAt(String str1,int pos)throws IOException{

		//FIRST WE HAVE TO CONVERT OUR STRING TO ARRAY USING PREDEFINED METHOD toCharArray()

		char arr[] = str1.toCharArray();

		return arr[pos];

	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER STRING :");
		String str1=br.readLine();

		System.out.println("ENTER POSITION FROM WHERE YOU WANT CHARACTER :");
		int pos=Integer.parseInt(br.readLine());

		char ret=myCharAt(str1,pos);

		System.out.println(ret);
	}
}


