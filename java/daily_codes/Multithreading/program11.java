

//*************************** CODE TO PREVENT THE DEADLOCK SCENARIO ***************************

class MyThread extends Thread{

	static Thread nmMain=null; 

	public void run(){

		try{
			nmMain.join(3000);  //THREAD-0 STOPS UNTIL MAIN COMPLETES ITS EXECUTION 

		}catch(InterruptedException ie){

		}
		
		for(int i=0;i<10;i++){

			System.out.println("IN THREAD-0");

		}
	}
}
class ThreadDemo{

	public static void main(String[] args) throws InterruptedException{

		MyThread.nmMain = Thread.currentThread();

		MyThread obj = new MyThread();
		obj.start();

		obj.join(1000); //MAIN STOPS UNTIL THREAD-0 COMPLETES ITS EXECUTION 

		for(int i=0;i<10;i++){

			System.out.println("In main");

		}
	}
}
