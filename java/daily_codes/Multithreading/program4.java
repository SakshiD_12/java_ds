
//PROGRAM TO CREATE NEW THREAD AND FROM THAT NEW THREAD AGAIN CREATING NEW THREAD 

class Demo extends Thread{

	public void run(){

		System.out.println("DEMO :"+Thread.currentThread().getName()); //THREAD-1

	}
}
class MyThread extends Thread{

	public void run(){

		System.out.println("MyThread :"+Thread.currentThread().getName()); //THREAD-0
		
		Demo obj = new Demo(); //THIS WILL CREATE ANOTHER THREAD 
		obj.start();

	}
}
class ThreadDemo{

	public static void main(String[] args){

		System.out.println("THREAD DEMO :"+Thread.currentThread().getName()); //MAIN

		MyThread obj = new MyThread();
		obj.start();

	}
}
