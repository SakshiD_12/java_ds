
//WE CANNOT START THE THREAD TWO TIMES ELSE IT WILL THROW IllegalThreadStateException 

class Demo extends Thread{

	public void run(){

		System.out.println(Thread.currentThread().getName());
		for(int i=0;i<10;i++){
			System.out.println("In Thread-0");
		}

	}
}
class Client{

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.start();
		obj.start();  // IllegalThreadStateException 

	
		for(int i=0;i<10;i++){
			System.out.println("In Thread-0");
		}
		
		System.out.println(Thread.currentThread().getName());

	}
}
