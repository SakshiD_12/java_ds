
//*********************** PROGRAM TO CREATE THREAD GROUP AND CHILD THREAD GROUP USING RUNNABLE ***********************


class MyThread implements Runnable{

	public void run(){

		System.out.println(Thread.currentThread());

	 	try{
		 	Thread.sleep(3000);

		}catch(InterruptedException ie){

			System.out.println(ie.toString());
		}	
	}
}
class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{

		ThreadGroup pThreadGP = new ThreadGroup("INDIA");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadGP,obj1,"MAHARASHTRA");
		Thread t2 = new Thread(pThreadGP,obj2,"MUMBAI");
		 
		t1.start();
		t2.start();

		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"pakistan");
		
		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread t3 = new Thread(cThreadGP,obj3,"karachi");
		Thread t4 = new Thread(cThreadGP,obj4,"lahore");
		 
		t3.start();
		t4.start();
	}
}

