

//SLEEP() METHOD THROWS INTERRUPTED EXCEPTION

class MyThread extends Thread{

	public void run(){ //here THREAD CLASS DOESNT THROWS ANY INTERRUPTED EXCEPTION IN METHOD RUN HENCE HERE ALSO OVERRIDED METHOD CANNOT DO THIS 

		for(int i=0;i<10;i++){

			System.out.println("IN RUN");

		}
		try{   // --------------------->HENCE INSTEAD OF THROWS WE HAVE TO USE TRY CATCH HERE
			Thread.sleep(1000);

		}catch(InterruptedException ie){


		}
	}
}
class ThreadDemo{
	public static void main(String[] args) throws InterruptedException{ //SLEEP THROWS INTERRUPTED EXCEPTION

		System.out.println("MAIN THREAD :"+Thread.currentThread().getName());

		MyThread obj = new MyThread();
		obj.start();

		for(int i=0;i<10;i++){
			 System.out.println("IN MAIN");
			 Thread.sleep(1000);

		}
	}
}


