
//JI PRIORITY PARENT CHI ASTE TICH PRIORITY CHILD CHI ASTE

class MyThread extends Thread{

	public void run(){

		Thread obj = Thread.currentThread();
		System.out.println(obj.getPriority()); //FOR THE OBJ1 = 5
		                                       //FOR OBJ2 = 7

	}
}
class ThreadDemo{

	public static void main(String[] args){

		Thread obj = Thread.currentThread();
		System.out.println(obj.getPriority()); //5

		MyThread obj1=new MyThread();
		obj1.start();

		obj.setPriority(7);

		MyThread obj2=new MyThread();
		obj2.start();

	}
}



