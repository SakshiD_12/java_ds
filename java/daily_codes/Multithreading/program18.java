
 
class MyThread extends Thread{

	MyThread(){


	}

	MyThread(String str){
		
		super(str);

	}
	public void run(){

		System.out.println(getName()); 
		
	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread("XYZ"); //XYZ
		obj.start();
		
		MyThread obj1 = new MyThread("PQR"); //PQR
		obj1.start();
		
		MyThread obj3 = new MyThread("PQR"); //PQR
		obj3.start();
		
		MyThread obj2 = new MyThread(); //Thread-0
		obj2.start();
		

	}
}
