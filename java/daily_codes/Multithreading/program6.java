
//PRIORITY OF THREADS 
//MINIMUM PRIORITY = 1
//MY DEFAULT PRIORITY = 5
//MAXIMUM PRIORITY = 10

//********************************* PROGRAM TO PRINT THE PRIORITY OF THREADS ***********************************

class ThreadDemo{
	public static void main(String[] args){

		Thread obj = Thread.currentThread(); //RETURN TYPE OF CURRENTTHREAD() IS THREAD CLASS OBJECT
		System.out.println(obj); //This will print Thread{main,5,main]---->Thread name, priority , Thread group

		System.out.println(obj.getName()); // THIS WILL PRINT THE THREAD NAME (main) 

		System.out.println(obj.getPriority()); //main has default priority as 5

		//WHEN WE ARE SETTING THE PRIORITY THE RANGE SHOULD BE FROM 1-10 ONLY ELSE IT WILL GIVE RUNTIME EXCEPTION(IllegalArgumentException)

		obj.setPriority(7);

		System.out.println(obj.getPriority()); // 7 


	}
}

