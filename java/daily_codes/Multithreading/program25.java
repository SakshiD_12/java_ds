
//******************** PROGRAM TO CREATE THE THREAD POOL ***********************

import java.util.concurrent.*;

class MyThread implements Runnable{

	int num;
	MyThread(int num){

		this.num=num;

	}
	public void run(){

		System.out.println(Thread.currentThread()+"START THREAD :"+num);

		dailyTask();

		System.out.println(Thread.currentThread()+"END THREAD :"+num);

	}
	void dailyTask(){

		try{
			Thread.sleep(8000);

		}catch(InterruptedException ie){

		}
	}
}
class ThreadPoolDemo{

	public static void main(String[] args){

		ExecutorService ser = Executors.newCachedThreadPool();  //THIS WILL CREATE THE THREADS AS PER THE NUMBER OF TASKS 

		//BELOW ARE THE 6 TASKS 
		for(int i=1;i<=6;i++){

			MyThread obj = new MyThread(i);
			ser.execute(obj); //passing runnable ofject to execute() method 

		}
		ser.shutdown(); //thredpool never stop hence to stop is shutdown is use

	}
}
