

//************************************ PROGRAM TO PRINT THR THREAD NAME ****************************************
//BUT THIS IS NOT A RECOMMENDED WAY TO CHANGE THE THREAD NAME 
class MyThread extends Thread{

	public void run(){

		System.out.println(getName()); //1.Thread-0 2.XYZ
		
	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread();
		obj.start();

		System.out.println(obj.getName()); //Thread-0 ( cause obj is the object of thread 0)
		
		obj.setName("XYZ"); //THIS WILL SET THE THREAD-0 NAME TO XYZ 

		System.out.println(obj.getName()); //XYZ
	}
}
