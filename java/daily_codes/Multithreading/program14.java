

//NOTE : THREAD GROUP IS ONE TYPE OF CLASS WHICH INCLUDES GROUP OF THREAD THAT CAN BE KILLED AND DESTROY USING SAME SINGLE COMMAND FOR ALL

//BY DEFAULT PRIOORITY OF THREAD GROUP IS 10

//************************************ PROGRAM TO PRINT THR THREADGROUP NAME ****************************************
class MyThread extends Thread{

	public void run(){

		System.out.println(Thread.currentThread().getThreadGroup());

	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
