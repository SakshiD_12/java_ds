
//************************************************ join() *****************************************************************

//IN JOIN ONE THREAD WAITS UNTIL ANOTHER THREAD COMPLETES ITS EXECUTION 

class MyThread extends Thread{

	public void run(){
		for(int i=0;i<10;i++){
			System.out.println("IN THREAD-0");
		}
	}
}
class ThreadDemo{
	public static void main(String[] args) throws InterruptedException { //JOIN THROWS INTERRUPTED EXCEPTION 

		MyThread obj = new MyThread();
		obj.start();

		obj.join(); //HERE MAIN THREAD WAITS UNTIL THREAD-0 COMPLETES ITS EXECUTION 

		for(int i=0;i<10;i++){
			System.out.println("In MAIN-THREAD");

		}
	}
}

