

// MULTITHREADING : THE PURPOSE OF MULTITHREADING IS TO INCREASE THE PERFORMANCE 
// MULTIPLE THREAD HAS BEEN CREATED TO FINISH THE TASK EARLIER
// JAVA HAS BY DEFAULT ONE THREAD WHICH IS MAIN THREAD
// FOR EVERY THREAD THERE IS ONE STACK FRAME ALSO COMES WITH IT
// THREAD SCHEDULER IS USED BY JVM TO MANAGE THE THREADS 
// THREAD SCHEDULER IS USED TO ALLOCATE THE CPU TO THE THREADS 
// WE CAN CREATE THE THREAD USING TWO METHODS : 1) USING THREAD CLASS
//                                              2) USING RUNNABLE INTERFACE 

//************************************************************ CREATING THREAD USING THREAD CLASS ********************************************************

class Mythread extends Thread{  //------------> HERE WE HAVE TO EXTEND THE THREAD CLASS BECAUSE METHOD USED TO CREATE THREAD IS PRESENT IN THE THREAD CLASS

	public void run(){ //-----> HERE WE ARE OVERRIDING THE RUN METHOD OF THREAD CLASS WHERE IN THREAD CLASS THAT METHOD IS PUBLIC HENCE HERE ALSO

	System.out.println("CHILD THREAD : "+ Thread.currentThread().getName());

		for(int i=0;i<10;i++){

			System.out.println("IN RUN");

		}
	}
}
class ThreadDemo{

	public static void main(String[] args){

		System.out.println("MAIN THREAD : "+Thread.currentThread().getName());

		Mythread obj = new Mythread(); //THIS LINE IS RESPONSIBLE FOR CREATING NEW THREAD  *** this line is executed by main thread 
		obj.start(); //START METHOD IS OF THE THREAD CLASS UNTIL WE START IT THREAD DIDNT START RUNNING

		for(int i=0;i<10;i++){

			System.out.println("IN MAIN");

		}
	}
}
