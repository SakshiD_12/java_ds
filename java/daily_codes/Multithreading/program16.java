

//************************************ PROGRAM TO PRINT THR THREAD NAME ****************************************
//RECOMMENDED WAY TO CHANGE THE THREAD NAME : AS PER THE COMAPANY PROTOTYPE ITS ALWAYS BETTER TO CHANGE THE NAME WHILE CREATIG THE OBJECT OF THR CLASS 
//THAT TIME PARAMETERIZED CONSTRUCTOR OF THREAD CLASS IS BEING CALLED AND FOR THAT WE HAVE TO WRITE THE PARAMETERIZED CONSTRUCTOR IN OUR CLASS AS IT CANNOT COMES BY DEFAULT 
//BUT WHENEVER WE PASS THE NAME WHILE CREATING THE OBEJCT THAT NAME SHOULG GO TILL THE THREAD CLASS CONSTRUCTOR SO WE USE SUPER TO PASS THE NAME FURTHER 
class MyThread extends Thread{

	MyThread(String str){
		
		super(str);

	}
	public void run(){

		System.out.println(getName()); // XYZ
		
	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread("XYZ");
		obj.start();
		
		System.out.println(obj.getName()); //XYZ

	}
}
