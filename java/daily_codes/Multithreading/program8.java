
//CONCURRENCY METHODS OF THREAD CLASS : 1) sleep() --------> it is a native method 
//                                      2) join() --------> it is also a native method
//                                      3) yield() 

//**************************************************** sleep() *******************************************************************

class MyThread extends Thread{

	public void run(){

		System.out.println(Thread.currentThread()); //[Thread-0,5,main]

	}
}
class ThreadDemo{
	public static void main(String[] args) throws InterruptedException{

		System.out.println(Thread.currentThread()); //[main,5,main]

		MyThread obj = new MyThread();
		obj.start();

		Thread.sleep(5000);

		Thread.currentThread().setName("c2w");
		System.out.println(Thread.currentThread()); //[c2w,5,main]

	}
}
