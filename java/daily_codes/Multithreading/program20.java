
//********************************* PROGRAM TO CREATE CHILD THREAD GROUP ************************************


class MyThread extends Thread{

	MyThread(ThreadGroup tg , String str){

		super(tg,str);

	}
	public void run(){

		System.out.println(Thread.currentThread());

	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup obj = new ThreadGroup("HYUNDAI"); //PARENT THREAD GROUP

		MyThread t1 = new MyThread( obj , "i20"); //passing threadgroup object and new threadgroup name to change 
		MyThread t2 = new MyThread( obj , "creta"); //passing threadgroup object and new threadgroup name to change 
		MyThread t3 = new MyThread( obj , "verna"); //passing threadgroup object and new threadgroup name to change 

		t1.start();
		t2.start();
		t3.start();
		
		ThreadGroup obj1 = new ThreadGroup(obj,"KIA"); //CHILD THREAD GROUP

		MyThread t4 = new MyThread(obj1,"kia Carnival");
		MyThread t5 = new MyThread(obj1,"kia Seltos");
		MyThread t6 = new MyThread(obj1,"kia EV9");

		t4.start();
		t5.start();
		t6.start();
		

	}
}
