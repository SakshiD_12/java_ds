
// NEVER OVERRIDE START METHOD OF THREAD CLASS because it is not that much strong to run our newly created thread

// NOTE : AND HERE THREAD-0 IS NOT CREATED MAIN THREAD IS ONLY RESPOSONSIBLE FOR EXECUTIING RUN METHOD
class MyThread extends Thread{

	public void run(){

		System.out.println("IN RUN");

		System.out.println(Thread.currentThread().getName());

	}
	public void start(){

		System.out.println("In MyThread Start");
		run();

	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());

	}
}
