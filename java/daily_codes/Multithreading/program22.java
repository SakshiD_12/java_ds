
//************************ PROGRAM TO INTERRUPT THE THREAD  **********************************

//INTERRUPT METHOD THROWS THE INTERRUPT FOR EVERY THREAD OF THE GROUP WHICH WE WRITE WHILE CALLING THE INTERRUPT METHOD 
class MyThread extends Thread{

	MyThread(ThreadGroup tg , String str){

		super(tg,str);

	}
	public void run(){

		System.out.println(Thread.currentThread());

	try{
		Thread.sleep(3000);

	}catch(InterruptedException ie){

		System.out.println(ie.toString());

	}

	}
}
class ThreadGroupDemo{

	public static void main(String[] args) throws InterruptedException { //interrupt() method throws interrupted exception 

		ThreadGroup pThreadGP = new ThreadGroup("INDIA"); //PARENT THREAD GROUP

		MyThread t1 = new MyThread(pThreadGP , "MAHARASHTRA");
		MyThread t2 = new MyThread(pThreadGP , "MUMBAI");

		t1.start();
		t2.start();

		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP , "PAKISTAN"); //CHILD THREAD GROUP 1

		MyThread t3 = new MyThread(cThreadGP , "KARACHI");
		MyThread t4 = new MyThread(cThreadGP , "LAHORE");

		t3.start();
		t4.start();

		ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP , "BANGLADESH"); //CHILD THREAD GROUP 2

		MyThread t5 = new MyThread(cThreadGP2 , "DHAKA");
		MyThread t6 = new MyThread(cThreadGP2 , "MIRPUR");

		t5.start();
		t6.start();

		cThreadGP.interrupt();

		System.out.println(pThreadGP.activeCount()); //THIS WILL RETURN THE ACTIVE ALL THREADS (6)
		System.out.println(pThreadGP.activeGroupCount());//AS WE CALL THE METHOD ON THE PARENT OBJECT IT WILL RETURN THR NO. OF CHILD OF THE PARENT

	}
}


