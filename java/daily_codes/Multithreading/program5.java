
//RUNNABLE ITERFACE IS A PARENT OF THREAD CLASS 
//IT IS IN THE LANG PACKAGE 
//RUNNABLE INTERFACE CONTAINS ONLY ONE METHOD THAT IS PUBLIC ABSTRACT RUN()
//RUNNABLE INTERFACE IS THE ROOT OF MULTITHREADING 
//WHEN WE ARE CREATING THREAD USING THE RUNNABLE INTERFACE WE NEED THREAD CLASS ALSO BECAUSE METHOD TO CREATE THREAD AND RUN THREAD IS IN THE THREAD CLASS 
 
//*********************************************** CREATING THREAD USING RUNNABLE INTERFACE **************************************************************

class MyThread implements Runnable{

	public void run(){

		System.out.println("IN RUN");
		System.out.println(Thread.currentThread().getName());

	}
}
class ThreadDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread();
		Thread t = new Thread(obj); //when we are creating thread using the runnable interface then we have to call the parameterised constructor 
		                           //of thread class thats why while creating object of thread class we need to pass the object of runnable 
					   //this all is need to take start and run method from the thread class 

		t.start();

		System.out.println(Thread.currentThread().getName());

	}
}
