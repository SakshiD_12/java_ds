
//********************************* PROGRAM TO CHANGE THE THREAD GROUP ************************************


class MyThread extends Thread{

	MyThread(ThreadGroup tg , String str){

		super(tg,str);

	}
	public void run(){

		System.out.println(Thread.currentThread());

	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup obj = new ThreadGroup("LOVE"); //RETURN TYPE IS OBJECT OF THREADGROUP CLASS 

		MyThread t1 = new MyThread( obj , "Sakshi"); //passing threadgroup object and new threadgroup name to change 
		MyThread t2 = new MyThread( obj , "Pratik"); //passing threadgroup object and new threadgroup name to change 
		MyThread t3 = new MyThread( obj , "Shinde"); //passing threadgroup object and new threadgroup name to change 
		MyThread t4 = new MyThread( obj , "Shinde"); //passing threadgroup object and new threadgroup name to change 

		t1.start();
		t2.start();
		t3.start();
		t4.start();
	}
}
