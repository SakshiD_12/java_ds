
// NOTE : NEVER USE JOIN() SIMPLY BECAUSE IT WILL CREATE THE DEADLOCK SITUATION LIKE IF WE WRITE JOIN IN MAIN ALSO AND THREAD-0 ALSO THEN SCHEDULER GETS CONFUSE ABOUT WHOM TO SCHEDLE HENCE ITS ALWYAS BETTER TO USE THE JOIN(LONG) WITH THE LONG PARAMETER as thread that joins on particular time will stop for the particular time only hence its alwyas recommended to use join(long) to prevent the deadlock 

//*************************** CODE THAT CREATES THE DEADLOCK SCENARIO ***************************

class MyThread extends Thread{

	static Thread nmMain=null; 

	public void run(){

		try{
			nmMain.join();  //THREAD-0 STOPS UNTIL MAIN COMPLETES ITS EXECUTION 

		}catch(InterruptedException ie){

		}
		
		for(int i=0;i<10;i++){

			System.out.println("IN THREAD-0");

		}
	}
}
class ThreadDemo{

	public static void main(String[] args) throws InterruptedException{

		MyThread.nmMain = Thread.currentThread();

		MyThread obj = new MyThread();
		obj.start();

		obj.join(); //MAIN STOPS UNTIL THREAD-0 COMPLETES ITS EXECUTION 

		for(int i=0;i<10;i++){

			System.out.println("In main");

		}
	}
}
