
//********************************************************** yield() **********************************************************

//THIS METHOS IS SAME AS THE JOIN BUT THE THREAD WAITS UNTIL ONLY ONE YIME QUANTUM FINISHES OF SNOTHER THREAD AFTER THAT BOTH OF THE THREAD ARE ON THE SAME LEVEL

class MYThread extends Thread{

	public void run(){

		//System.out.println(Thread.currentThread().getName());
		for(int i=0;i<10;i++){
			System.out.println("In Thread-0");
		}
	}
}
class ThreadDemo1{

	public static void main(String[] args){

		MYThread obj = new MYThread();
		obj.start();

		obj.yield();

		//System.out.println(Thread.currentThread().getName());
		for(int i=0;i<10;i++){
			System.out.println("In Main");
		}

	}
}
