
//WAP TO PRINT THE SPELLING OF 0 TO 5 AND IF NUMBER IS GREATER THAN 5 WRITE APPROPRIATE CONDITION

class Number{

	public static void main(String[] args){

		int var=-5;

		if(var<=5 && var>=0){

			if(var==0){
				System.out.println("zero");
			}else if(var==1){

				System.out.println("ONE");
			}else if(var==2){

				System.out.println("TWO");
			}else if(var==3){

				System.out.println("THREE");
			}else if(var==4){

				System.out.println("FOUR");
			}else if(var==5){

				System.out.println("FIVE");
			}
		}else if(var>5){

				System.out.println(var+" IS GREATER THAN 5");
		}else{

				System.out.println("INVALID NUMBER");
		}
	}
}
