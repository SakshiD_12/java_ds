
//A b C d
//E f G h
//I j K l
//M n O p
import java.util.*;
class Number{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		char ch='A';
		char ch1='a';
		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=row;j++){
				
				if(j%2==1){

					System.out.print(ch+" ");
				
				}else{
					System.out.print(ch1+" ");

				}
				ch++;
				ch1++;	

			}
			System.out.println();
		}
	}
}

