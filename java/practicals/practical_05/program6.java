
//1 4 9
//16 25 36
//49 64 81

import java.util.*;
class Number{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int x=1;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){

				System.out.print(x*x+" ");
				x++;

			}
			System.out.println();
		}
	}
}

