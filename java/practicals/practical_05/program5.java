
//26 z 25 y
//24 x 23 w
//22 v 21 u
//20 t 19 s

import java.util.*;
class Number{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int x=row*row+10;
		char ch='Z';
		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=row;j++){
				if(j%2==1){

				   	System.out.print(x+" ");
					x--;
				}else{
					System.out.print(ch+" ");
					ch--;
				}

			}
			System.out.println();
		}
	}
}

