
//1 2 9
//4 25 6
//49 8 81

import java.util.*;
class Number{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();
		
		int x=1;

		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=row;j++){
				
				if(x%2==1){
					System.out.print(x*x+" ");

				}else{
					System.out.print(x+" ");

				}
				x++;

			}
			System.out.println();
		}
	}
}

