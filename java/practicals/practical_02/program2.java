

//WAP TO CHECK WETHER THE NUMBER IS AUTOMORPHIC NUMBER OR NOT

//AUTOMORPHIC NUMBER ------->LAST DIGIT OF THE THE SQUARE OF PARTICULAR NUMBER IS EQUAL TO THAT NUMBER ONLY


//ex-------> 5 = 25 (last digit of square of 5 is 5 only)
//--------->76 = 5776(76)


class Number{

	public static void main(String[] args){

		int N=76;
		int temp1=N;
		int sum=0;
		int count=0;
		int mult=N*N;
		int rev=0;
	

		while(N != 0){

			count++; //2
			N=N/10; //returns the quotient

		}
		for(int i=1;i<=count;i++){

			int rem=mult%10; //returns the remainder
			sum=(sum*10)+rem; //67
			mult=mult/10;

		}
		while(sum != 0){

			int rem =sum%10; //76
		        rev=(rev*10)+rem; //76
			sum=sum/10;

		}
		if(temp1==rev){ 
			System.out.println(temp1+ " is automorphic number");

		}else{
			System.out.println(temp1+ " is not automorphic number");

		}
	}
}



