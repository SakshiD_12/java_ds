
//C B A
//C B A
//C B A

import java.util.*;
class Number{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		for(int i=1;i<=row;i++){
			
			int ch=65+(row-1);

			for(int j=1;j<=row;j++){

				System.out.print((char)ch+" ");
				ch--;

			}
			System.out.println();
		}


	}
}
