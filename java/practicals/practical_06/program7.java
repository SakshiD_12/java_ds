

//F
//E F
//D E F
//C D E F
//B C D E F
//A B C D E F
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();
		
		int ch=65+(row-1);

		for(int i=1;i<=row;i++){
			
			int temp=ch;

			for(int j=1;j<=i;j++){

				System.out.print((char)temp+" ");
				temp++;
				

			}
			ch--;

			System.out.println();
		}
	}
}
