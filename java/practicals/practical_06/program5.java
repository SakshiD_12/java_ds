
//10 10 10 10
//11 11 11
//12 12
//13
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();
		
		int x=10;

		for(int i=1;i<=row;i++){

			for(int j=row;j>=i;j--){

				System.out.print(x+" ");

			}
			x++;
			System.out.println();
		}
	}
}
