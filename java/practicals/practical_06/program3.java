
//10
//9 8 
//7 6 5
//4 3 2 1
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();
		
		int x=(row*(row+1))/2;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				System.out.print(x+" ");
				x--;

			}
			System.out.println();
		}
	}
}
