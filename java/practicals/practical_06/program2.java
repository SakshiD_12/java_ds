
//1
//2 3
//4 5 6
//7 8 9 10
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();
		
		int x=1;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				System.out.print(x+" ");
				x++;

			}
			System.out.println();
		}
	}
}
