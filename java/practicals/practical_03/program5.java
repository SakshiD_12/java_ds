//WAP TO PRINT THE SQUARE OF THE EVEN DIGIT OF NUMBER 

class Number{

	public static void main(String[] args){

		int N=43632;
		int count=0;
		
		while(N!=0){

			int rem=N%10;  //------>returns the reminder 
			N=N/10;
			if(rem%2==0){

			  System.out.println("THE SQUARE OF"+" "+rem+" is "+rem*rem);

			}

		}

	}
}
