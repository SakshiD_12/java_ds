//WAP TO PRINT THE NUMBER OF DIGITS IN THE NUMBER 


class Number{

	public static void main(String[] args){

		int N=123456;
		int count=0;
		
		while(N!=0){

			int rem=N%10;  //------>returns the reminder 6 5 4 3 2 1 
			N=N/10;        //----->divide the number and returns the quotient 
			count++;

		}

		System.out.println("THE NUMBER OF DIGITS ARE :"+count);

	}
}
