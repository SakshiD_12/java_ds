

//0 
//1 1
//2 3 5
//8 13 21 34

import java.io.*;
class Number{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = Integer.parseInt(br.readLine());
		
		int a=0,b=1;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

			System.out.print(a+" ");
			int temp=a+b;
			a=b;
			b=temp;

			}
			System.out.println();
		}
	}
}

