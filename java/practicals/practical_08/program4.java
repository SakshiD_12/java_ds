

//TAKE A RANGE FROM USER AND PRINT EVEN NUMBERS IN REVERSE ORDER AND ODD NUMBERS IN STANDARD WAY WITJIN RANGE
//  INPUT----> START RANGE = 2
//       ----> END RANGE   =9
//
//  OUTPUT---->8 9 4 2
//             3 5 7 9


import java.io.*;
class Number{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER START AND END RANGE :");
		int s=Integer.parseInt(br.readLine());

		int e=Integer.parseInt(br.readLine());
	
		System.out.print("EVEN NUMBERS :");
		for(int i=e;i>=s;i--){

			if(i%2==0){

				System.out.print(i+" ");

			}
		}
		System.out.println();
		System.out.print("ODD NUMBERS :");
		for(int i=s;i<=e;i++){

			if(i%2==1)
			{

				System.out.print(i+" ");
			}
		}
		System.out.println();
	}
}

