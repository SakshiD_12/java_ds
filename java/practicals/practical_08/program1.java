

//D4 C3 B2 A1
//A1 B2 C3 D4
//D4 C3 B2 A1
//A1 B2 C3 D4

import java.io.*;
class Number{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){

				if(i%2==1){

					System.out.print((char)(65+row-j)+""+(row-j+1)+" "); 

				}else{
					System.out.print((char)(64+j)+""+(j)+" ");

				}

			}
			System.out.println();
		}
	}
}

