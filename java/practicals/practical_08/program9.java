

//WAP TO TAKE NMBER FROM USER AND PRINT THE ADDITION OF FACTORAL OF EACH DIGIT OF THAT NUMBER

import java.io.*;
class Number{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER NUMBER :");
		int n = Integer.parseInt(br.readLine());

		int sum=0;
		while(n != 0){

  			int rem=n%10;
			int fact=1;

			while(rem != 0){

				fact=fact*rem;
				rem--;

			}
			sum=sum+fact;
			n=n/10;
		}
		System.out.println("THE ADDITION OF FACTOREAL OF EACH DIGIT IS :"+sum);
	}
}
