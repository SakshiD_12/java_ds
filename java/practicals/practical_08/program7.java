

//0
//14 13
//L K J
//9 8 7 6
//E D C B A
import java.io.*;
class Number{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = Integer.parseInt(br.readLine());
		
		int x=(row*(row+1))/2;
		int ch=65+(x-1);

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				if(i%2==1 && row%2==1 || i%2==0 && row%2==0){

					System.out.print((char)ch+" ");

				}else{
					System.out.print(x+" ");

				}
				ch--;
				x--;

			}
			System.out.println();
		}
	}
}

