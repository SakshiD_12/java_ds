

//1 2 3 4
//2 3 4
//3 4
//4

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int x=1;

		for(int i=1;i<=row;i++){
			
			int temp=x;

			for(int j=row;j>=i;j--){

				System.out.print(temp+" ");
				temp++;

			}
			x++;
			System.out.println();

		}
	}
}
