

//10
//I H
//7 6 5
//D C B A
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int x= (row*(row+1))/2;
		int ch=65+((row*2)+1);

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				if(i%2==1){

					System.out.print(x+" ");
					
				}else{
					System.out.print((char)ch+" ");

				}
				x--;
				ch--;

			}
			System.out.println();

		}
	}
}
