

//F
//E 1
//D 2 E
//C 3 D 4
//B 5 C 6 D
//A 7 B 8 C 9
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

	        int ch=65+(row-1);
		int x=1;

		for(int i=1;i<=row;i++){
				
			int temp=ch;

			for(int j=1;j<=i;j++){
				
				if(j%2==1){

						System.out.print((char)temp+" ");
						temp++;

				}else{
						System.out.print(x+" ");
						x++;

				}

			}
			ch--;
			System.out.println();

		}
	}
}
