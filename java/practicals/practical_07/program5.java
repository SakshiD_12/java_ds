

//A B C D
//B C D
//C D
//D

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int ch=65;

		for(int i=1;i<=row;i++){
				
			int temp=ch;

			for(int j=row;j>=i;j--){

				System.out.print((char)temp+" ");
				temp++;

			}
			ch++;
			System.out.println();

		}
	}
}
