

//C2W10
//C2W9 C2W8
//C2W7 C2W6 C2W5
//C2W4 C2W3 C2W2 C2W1

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int x= (row*(row+1))/2;

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){

				System.out.print("C2W"+x+" ");
				x--;

			}
			System.out.println();

		}
	}
}
