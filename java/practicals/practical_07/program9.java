

//1 
//8 9
//27 16 125
//64 25 216 49
import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("ENTER NUMBER OF ROWS :");
		int row = obj.nextInt();

		int x=1;

		for(int i=1;i<=row;i++){
			
			int temp=x;

			for(int j=1;j<=i;j++){
				
				if(j%2==1){

					System.out.print(temp*temp*temp+" ");
					temp++;

				}else{
					System.out.print(temp*temp+" ");
					temp++;

				}

			}
			x++;
			System.out.println();

		}
	}
}
