
//WAP TO PRINT ONLY THOSE ELEMENTS FROM ARRAY WHOSE SUM OF DIGIT IS EVEN 

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		
		int sum=0;

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
			
			int N=arr[i];
			int sum2=0;

			while(N!=0){

				int rem=N%10;
				sum2=sum2+rem;
				N=N/10;

			}

			if(sum2%2==0){

				sum=sum+arr[i];
			}
		}

		System.out.println(+sum);
	}
}

