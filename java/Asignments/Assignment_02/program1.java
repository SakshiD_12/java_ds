
//WAP TO TAKE ARRAY ELEMET AND SIZE FROM USER AND PRUNT THE SUM OF ARRAY ELEMENTS 

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

	BufferedReader br=new BufferedReader( new InputStreamReader(System.in));

	System.out.println("ENTER ARRAY SIZE :");
	int size = Integer.parseInt(br.readLine());
	
	int arr[]=new int[size];

	int sum=0;

	System.out.println("ENTER ARRAY ELEMENTS :");
	for(int i=0;i<arr.length;i++){

		arr[i]=Integer.parseInt(br.readLine());

		sum=sum+arr[i];

	}
	System.out.println("THE SUM OF ARRAY ELEMENTS IS :"+sum);
	}
}

