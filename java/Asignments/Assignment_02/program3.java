
//WAP TO TAKE ARRAY ELEMET AND SIZE FROM USER AND PRINT THE SUM OF EVEN AND ODD ELEMENTS FROM THE ARRAY 
import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

	BufferedReader br=new BufferedReader( new InputStreamReader(System.in));

	System.out.println("ENTER ARRAY SIZE :");
	int size = Integer.parseInt(br.readLine());
	
	int arr[]=new int[size];

	int sumE=0,sumO=0;

	System.out.println("ENTER ARRAY ELEMENTS :");
	for(int i=0;i<arr.length;i++){

		arr[i]=Integer.parseInt(br.readLine());
		
		if(arr[i]%2==0){

			sumE=sumE+arr[i];

		}else{
			sumO=sumO+arr[i];

		}

	}
	System.out.println("THE SUM OF EVEN ARRAY ELEMENTS IS :"+sumE);
	System.out.println("THE SUM OF ODD ARRAY ELEMENTS IS :"+sumO);
	}
}

