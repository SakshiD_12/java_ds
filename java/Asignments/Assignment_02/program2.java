
//WAP TO count number of even and odd integers from array

import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

	BufferedReader br=new BufferedReader( new InputStreamReader(System.in));

	System.out.println("ENTER ARRAY SIZE :");
	int size = Integer.parseInt(br.readLine());
	
	int arr[]=new int[size];

	int count1=0,count2=0;

	System.out.println("ENTER ARRAY ELEMENTS :");
	for(int i=0;i<arr.length;i++){

		arr[i]=Integer.parseInt(br.readLine());

		if(arr[i]%2==0){

			count1++;

		}else{
			count2++;

		}
	}

	System.out.println("THE NUMBER OF EVEN ELEMENTS IS :"+count1);
	System.out.println("THE NUMBER OF ODD ELEMENTS IS :"+count2);
	}
}

