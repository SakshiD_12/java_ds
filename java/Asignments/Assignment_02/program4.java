
//WAP TO SEARCH SPECIFIC ELEMENT FROM THE ARRAY AND RETURN ITS INDEX 
import java.io.*;

class Demo{

	public static void main(String[] args) throws IOException{

	BufferedReader br=new BufferedReader( new InputStreamReader(System.in));

	System.out.println("ENTER ARRAY SIZE :");
	int size = Integer.parseInt(br.readLine());
	
	int arr[]=new int[size];


	System.out.println("ENTER ARRAY ELEMENTS :");
	for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
	
	System.out.println("ENTER ELEMENT TO SEARCH :");
	int search=Integer.parseInt(br.readLine());
	

	for(int i=0;i<arr.length;i++){

		if(arr[i]==search){

			System.out.println("ELEMENT FOUND AT INDEX :"+i);

		}
	}
}
}

