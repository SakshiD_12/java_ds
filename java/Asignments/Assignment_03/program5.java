
//WAP TO RETURN THE INDEX OF PERFECT NUMBER IN ARRAY(THE ADDITION OF FACTORS EXCEPT THAT NUMBER )
import java.io.*;

class Demo{
	
	void PerfectDigit(int arr[]){

		for(int i=0;i<arr.length;i++){
			
			int sum=0;
			
			for(int j=1;j<arr[i];j++){

				if(arr[i]%j==0){

					sum=sum+j;

				}

			}

				if(sum==arr[i]){
					System.out.println("PERFECT NUMBER "+arr[i]+" IS FOUND AT INDEX "+i);
				
				}

		}
	}

	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		Demo obj=new Demo();

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		obj.PerfectDigit(arr);
		
	}
}

