
//WAP TO RETURN THE INDEX OF PALINDROME NUMBER FROM AN ARRAY 
import java.io.*;

class Demo{
	
	void PalindromeDigit(int arr[]){

		for(int i=0;i<arr.length;i++){

			int sum=0;
			int N=arr[i];
			int rev=0;

			while(N!=0){

				int rem=N%10;
				rev=(rev*10)+rem;
				N=N/10;

			}
			if(rev==arr[i]){

				System.out.println("PALINDROME ELEMENT "+arr[i]+" FOUND AT INDEX "+i);

			}
	}

	}

	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		Demo obj=new Demo();

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		obj.PalindromeDigit(arr);
		
	}
}

