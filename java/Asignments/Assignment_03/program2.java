
//WAP TO REVERSE EACH ELEMENT OF ARRAY
// I/P-----> 02 255 2 1554
// O/P----->20 552 2 4551

import java.io.*;

class Demo{

	static int ReverseDigit(int N){

		int rev=0;

		while(N!=0){

			int rem=N%10;
			rev=(rev*10)+rem;
			N=N/10;

		}

		return rev;

	}
	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
		System.out.println("ELEMENTS BEFORE REVERSING ARRAY :");
		for(int i=0;i<arr.length;i++){

			System.out.println(arr[i]);

		}
		System.out.println("ELEMENTS AFTER REVERSING ARRAY :");
		for(int i=0;i<arr.length;i++){

			System.out.println(ReverseDigit(arr[i]));

		}
	}
}


