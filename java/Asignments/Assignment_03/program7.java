
//WAP TO RETURN THE INDEX OF STRONG NUMBER FROM AN ARRAY(ADDITION OF FACTORIAL IS EQUAL TO THAT NUMBER)

import java.io.*;

class Demo{
	
	void StrongDigit(int arr[]){

		for(int i=0;i<arr.length;i++){
			
			int N=arr[i];
			int sum=0;
			
			while(N!=0){

				int rem=N%10;
				int fact=1;

				for(int j=1;j<=rem;j++){

					fact=fact*j;
				}

				sum=sum+fact;
				N=N/10;
			}
			if(sum==arr[i]){

				System.out.println("STRONG DIGIT "+arr[i]+" FOUND AT INDEX "+i);

			}
		}
	}

	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		Demo obj=new Demo();

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		obj.StrongDigit(arr);
		
	}
}

