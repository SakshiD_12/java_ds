
//WAP TO RETURN THE INDEX OF ARMSTRONG NUMBER FROM AN ARRAY

import java.io.*;

class Demo{
	
	void ArmstrongDigit(int arr[]){

		for(int i=0;i<arr.length;i++){
			
			int N=arr[i];
			int sum=0,count=0;
			
			while(N!=0){

				count++;
				N=N/10;

			}
			int Num=arr[i];
			while(Num!=0){
				int mult=1;
				int rem=Num%10;
				for(int j=1;j<=count;j++){

					mult=mult*rem;
				}
				sum=sum+mult;
				Num=Num/10;
			}
			if(sum==arr[i]){
				System.out.println("Palindrome Number "+arr[i]+" Found At "+i);
			}
	}
	}

	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		Demo obj=new Demo();

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		obj.ArmstrongDigit(arr);
		
	}
}

