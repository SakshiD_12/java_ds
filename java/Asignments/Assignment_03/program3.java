
//WAP TO FIND COMPOSITE NUMBER FROM ARRAY AND RETURN ITS INDEX 

import java.io.*;

class Demo{

	void CompositeDigit(int arr[]){
	
		int flag=0;
		for(int i=0;i<arr.length;i++){

			int count=0;

			for(int j=1;j*j<=arr[i];j++){

				if(arr[i]%j==0){
					count+=2;
				}
				if(count>2){
					break;

				}
			}
			if(count>2){
				flag=1;
				System.out.println("COMPOSITE ELMENET "+arr[i]+" IS FOUNT AT INDEX "+i);
			}
		}
		if(flag==0){
			System.out.println("NO COMPOSITE LEMENT FOUND IN ARRAY.....");
		}
	}
	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		Demo obj=new Demo();

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
		obj.CompositeDigit(arr);
	}
}

