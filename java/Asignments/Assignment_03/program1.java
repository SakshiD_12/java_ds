
//WAP TO PRINT THE COUNT OF DIGIT IN ARRAY ELEMENT 
// I/P-----> 02 255 2 1554
// O/P----->2 3 1 4

import java.io.*;

class Demo{

	static int CountDigit(int N){

		int count=0;

		while(N!=0){

			count++;
			N=N/10;

		}

		return count;

	}
	public static void main(String[] args) throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER ARRAY SIZE :");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
		System.out.println("COUNT OF DIGIT :");
		for(int i=0;i<arr.length;i++){

			System.out.println(CountDigit(arr[i])+ " ");

		}
	}
}

