
//WAP TO TAKE SIZE AN ARRAY ELEMENTS FROM THE USER AND PRINT SUM OF ODD ELEMENTS ONLY

import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER ARRAY SIZE :");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		int sum=0;

		System.out.println("ENTER ARRAY ELEMENT TO GET THE SUM OF ADD ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
			
			if(arr[i]%2==1){

				sum=sum+arr[i];

			}
		}
		System.out.println("THE SUM OF ODD ELEMENT IS :"+sum);

	}
}
