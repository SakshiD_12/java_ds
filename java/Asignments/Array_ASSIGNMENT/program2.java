
//WAP TO TAKE SIZE AN ARRAY ELEMENTS FROM THE USER AND PRINT product OF EVEN ELEMENTS ONLY

import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER ARRAY SIZE :");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		int prod=1;

		System.out.println("ENTER ARRAY ELEMENT TO GET THE PRODUCT OF ADD ELEMENTS :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
			
			if(arr[i]%2==0){

				prod=prod*arr[i];

			}
		}
		System.out.println("THE PRODUCT OF EVEN ELEMENT IS :"+prod);

	}
}
