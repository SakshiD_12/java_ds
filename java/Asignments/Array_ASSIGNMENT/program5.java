
//WAP TO TAKE SIZE AN ARRAY ELEMENTS FROM THE USER AND PRINT ONLY ELEMENTS WHICH IS DIVISIBLE BY 5

import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENTER ARRAY SIZE :");
		int size = Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENTER ARRAY ELEMENT TO GET THE ELEMENTS WHIC IS DIVISIBLE BY 5  :");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
			
		}
		System.out.println("ELEMENTS THAT ARE DIVISIBLE BY 5 ARE : ");
		for(int i=0;i<arr.length;i++){

			if(arr[i]%5==0){

				System.out.println(arr[i]);
			}
		}
	}
}
