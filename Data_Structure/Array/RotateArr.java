
//Array Rotation 

class Rotation{

	static int[] rotateArr(int arr[],int count){

		int tempArr[] = new int[arr.length];
		
		int rotate=(count  %  arr.length);

		int itr=0;

		for(int i=0;i<arr.length;i++){

			if(rotate + i < arr.length){

				tempArr[i+rotate]=arr[i];

			}else{
				tempArr[itr]=arr[i];
				itr++;

			}

		}

		return tempArr;

	}

	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,4,5};

		int count=2;

		System.out.println("ARRAY ELEMENTS BEFORE ROTATING :");
		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+" ");

		}

		System.out.println();

		int retArr[]=rotateArr(arr,count);

		System.out.println("ARRAY ELEMENTS AFTER ROTATING :");
		for(int i=0;i<retArr.length;i++){

			System.out.print(retArr[i]+" ");
	
		}
		System.out.println();
	}
}




