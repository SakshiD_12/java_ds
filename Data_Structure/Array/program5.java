
//PRINT THE SUM OF ARRAY ELEMENTS IN FIRST AND LAST MANNER 

class SumN{

	public static void main(String[] args){

		int arr[]= new int[]{1,2,3,4,6,7,8};

		int start=0;
		int end=arr.length-1;

		while(start<=end){

			if(start==end){
				System.out.print(arr[start]+" ");

			}else{
				System.out.print(arr[start]+arr[end]+" ");

			}
			start++;
			end--;

		}

		System.out.println();
	}
}
