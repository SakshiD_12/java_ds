
//GIVEN ARRAY OF SIZE N COUNT THE NUMBER OF ELEMENTS HAVING ATLEAST 1 ELEMENT GREATER THAN IT


//********************************** OPTIMIZED APPROACH *************************************

class ArrDemo{

	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,4,5};

		int count=0;

		int N=arr.length;

		int max=Integer.MIN_VALUE;


		for(int i=0;i<arr.length;i++){

			if(arr[i]>max){

				max=arr[i];

			}

		}

		for(int i=0;i<arr.length;i++){
			if(arr[i]==max){

				count++;

			}
		}

		int c=N-count;

		System.out.println("COUNT IS :"+c);

	}
}
