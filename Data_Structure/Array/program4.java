
//PRINT THE SECOND ARGEST ELEMENT IN AN ARRAY

class SecondLarge{

	public static void main(String[] args){

		int arr[]= new int[]{1,2,3,4,5,6,7,8};

		int max=Integer.MIN_VALUE;
		int secMax=max;

		for(int i=0;i<arr.length;i++){

			if(arr[i]>max){
				secMax=max;
				max=arr[i];
			}else if(arr[i] <max && arr[i]>secMax){

				secMax=arr[i];

			}
		}
		System.out.println(secMax);

	}
}
