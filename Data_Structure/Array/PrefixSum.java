
//GIVEN AN ARRAY OF SIZE N  AND Q QUERY NUMBER OF QUERIES CONTAINS TWO PARAMETER (s,e) FOR ALL QUERIES PRINT THE SUM OF ALL ELEMENTS IN GIVEN RANGE 


import java.util.Scanner;

class PrefixS{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);

		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int N=10;
		int Q=3;

		int PSarr[] = new int[N];

		PSarr[0]=arr[0];

		for(int i=1;i<N;i++){

			PSarr[i]=PSarr[i-1]+arr[i];

		}

		int sum=0;
		for(int i=0;i<Q;i++){

			System.out.println("ENTER START INDEX :");
			int s= sc.nextInt();

			System.out.println("ENTER END INDEX :");
			int e=sc.nextInt();

			if(s==0){
				sum=PSarr[e];

			}else{
				sum=PSarr[e]-PSarr[s-1];

			}
			System.out.println(sum);

		}
	}
}



