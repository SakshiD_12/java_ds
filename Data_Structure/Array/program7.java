
//SUM OF ARRAY ELEMENT IN GIVEN RANGE 

//******************* brute force approach(for 1 query)

import java.util.Scanner;
class ArrDemo{

	public static void main(String[] args){
	
		Scanner sc= new Scanner(System.in);

		int arr[]=new int[]{1,2,3,4,5};

		System.out.println("ENTER START :");
		int s=sc.nextInt();

		System.out.println("ENTER END :");
		int e=sc.nextInt();
	
		int sum=0;
		for(int i=s;i<=e;i++){

			sum=sum+arr[i];

		}

		System.out.println(sum);

	}
}
